# Meow! Install

A title manager for the Switch, written in Rust by EliseZeroTwo

## Features

* Installing to NAND or SD
* NSP Installation
* Unpacked NSP Installation
* Backing up titles as an NSP to SD

## Supported FW

This was tested on 16.0.3

## Credits

* [aarch64-switch-rs](https://github.com/aarch64-switch-rs/) project's work to bring Rust to Horizon
