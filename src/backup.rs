pub mod es;
pub mod sd;

use core::fmt::Display;

use alloc::vec::Vec;
use nx::ipc::sf::{Buffer, EnumAsPrimitiveType};

use crate::hos::{
    hipc::ncm::{
        get_ncm_session, ContentInstallType, ContentMetaKey, ContentMetaType, IContentMetaDatabase,
        ProgramId, StorageId,
    },
    ncm::NcmContentInfo,
};

pub type BackupResult<T> = core::result::Result<T, BackupError>;

#[derive(Debug)]
pub enum BackupError {
    Ticket(es::FetchTicketError),
    Nx(nx::result::ResultCode),
}

impl Display for BackupError {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            BackupError::Ticket(ticker_err) => ticker_err.fmt(f),
            BackupError::Nx(nx) => nx.fmt(f),
        }
    }
}

impl From<es::FetchTicketError> for BackupError {
    fn from(value: es::FetchTicketError) -> Self {
        Self::Ticket(value)
    }
}

impl From<nx::result::ResultCode> for BackupError {
    fn from(value: nx::result::ResultCode) -> Self {
        Self::Nx(value)
    }
}

/// Fetch a list of [NcmContentInfo] for every NCA we will backup
fn fetch_content_info_list(
    storage_id: StorageId,
    program_id: ProgramId,
) -> BackupResult<Option<Vec<NcmContentInfo>>> {
    let content_meta_db = get_ncm_session()?
        .get()
        .open_content_meta_database(storage_id)?;

    let mut buffer: Vec<ContentMetaKey> = alloc::vec![ContentMetaKey::default(); 0x80];
    let (_total_count, read_count) = content_meta_db.get().content_meta_database_list(
        Buffer::from_mut_array(&mut buffer),
        EnumAsPrimitiveType::from(ContentMetaType::Application),
        program_id,
        program_id,
        program_id,
    )?;

    let key = match buffer.into_iter().take(read_count as usize).find(|key| {
        key.content_install_type == ContentInstallType::Full
            && key.content_meta_type == ContentMetaType::Application
    }) {
        Some(key) => key,
        None => {
            return Ok(None);
        }
    };

    let mut buffer = alloc::vec![NcmContentInfo::default(); 0x80];
    let read_count = content_meta_db
        .get()
        .content_meta_database_list_content_info(Buffer::from_mut_array(&mut buffer), key, 0)?;

    buffer.resize_with(read_count.max(0) as usize, || {
        unreachable!("read_count will always be less than the buffer length")
    });
    Ok(Some(buffer))
}
