use core::fmt::Display;

use alloc::{format, vec::Vec};
use fatfs::Read;
use nx::{
    ipc::sf::{Buffer, EnumAsPrimitiveType},
    mem::Shared,
    result::Result,
    service::fsp::{BisPartitionId, IStorage},
};

use crate::{
    hos::hipc::{
        es::{get_es_session, RightsId},
        ncm::ProgramId,
    },
    ui::logger_task::LoggerTx,
};

struct FatFsWrapper {
    fs: Shared<dyn IStorage>,
    offset: i64,
    len: i64,
}

impl FatFsWrapper {
    pub fn new(fs: Shared<dyn IStorage>) -> Result<Self> {
        let len = fs.get().get_size()?;
        Ok(Self { fs, offset: 0, len })
    }
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum WrappedIoError {
    Nx(nx::result::ResultCode),
    UnexpectedEof,
    WriteZero,
    BadOffset,
}

impl Display for WrappedIoError {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            WrappedIoError::Nx(nx) => f.write_fmt(format_args!("Nx({})", *nx)),
            WrappedIoError::UnexpectedEof => f.write_str("UnexpectedEof"),
            WrappedIoError::WriteZero => f.write_str("WriteZero"),
            WrappedIoError::BadOffset => f.write_str("BadOffset"),
        }
    }
}

impl fatfs::IoError for WrappedIoError {
    fn is_interrupted(&self) -> bool {
        matches!(self, Self::Nx(_))
    }

    fn new_unexpected_eof_error() -> Self {
        Self::UnexpectedEof
    }

    fn new_write_zero_error() -> Self {
        Self::WriteZero
    }
}

impl fatfs::IoBase for FatFsWrapper {
    type Error = WrappedIoError;
}

impl From<nx::result::ResultCode> for WrappedIoError {
    fn from(value: nx::result::ResultCode) -> Self {
        Self::Nx(value)
    }
}

impl fatfs::Seek for FatFsWrapper {
    fn seek(&mut self, pos: fatfs::SeekFrom) -> core::result::Result<u64, Self::Error> {
        let requested_offset = match pos {
            fatfs::SeekFrom::Start(offset) => offset as i64,
            fatfs::SeekFrom::End(offset) => self.len + offset,
            fatfs::SeekFrom::Current(offset) => self.offset + offset,
        };

        if !requested_offset.is_negative() && requested_offset < self.len {
            self.offset = requested_offset;
            Ok(requested_offset as u64)
        } else {
            Err(WrappedIoError::BadOffset)
        }
    }
}

impl fatfs::Read for FatFsWrapper {
    fn read(&mut self, buf: &mut [u8]) -> core::result::Result<usize, Self::Error> {
        if self.offset as usize + buf.len() >= self.len as usize {
            return Err(WrappedIoError::BadOffset);
        }

        self.fs
            .get()
            .read(self.offset, buf.len() as u64, Buffer::from_mut_array(buf))?;

        self.offset += buf.len() as i64;

        Ok(buf.len())
    }
}

impl fatfs::Write for FatFsWrapper {
    fn write(&mut self, buf: &[u8]) -> core::result::Result<usize, Self::Error> {
        if self.offset as usize + buf.len() >= self.len as usize {
            return Err(WrappedIoError::BadOffset);
        }

        self.fs
            .get()
            .write(self.offset, buf.len() as u64, Buffer::from_array(buf))?;

        self.offset += buf.len() as i64;

        Ok(buf.len())
    }

    fn flush(&mut self) -> core::result::Result<(), Self::Error> {
        self.fs.get().flush()?;

        Ok(())
    }
}

#[derive(Debug)]
pub enum FetchTicketError {
    FatFs(fatfs::Error<WrappedIoError>),
    Nx(nx::result::ResultCode),
}

impl Display for FetchTicketError {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        match self {
            FetchTicketError::FatFs(err) => err.fmt(f),
            FetchTicketError::Nx(err) => err.fmt(f),
        }
    }
}

impl From<nx::result::ResultCode> for FetchTicketError {
    fn from(value: nx::result::ResultCode) -> Self {
        Self::Nx(value)
    }
}

impl From<fatfs::Error<WrappedIoError>> for FetchTicketError {
    fn from(value: fatfs::Error<WrappedIoError>) -> Self {
        Self::FatFs(value)
    }
}

pub fn fetch_ticket(
    tx: &LoggerTx,
    program_id: ProgramId,
) -> core::result::Result<Option<(RightsId, Vec<u8>)>, FetchTicketError> {
    let fs = nx::fs::get_fspsrv_session()?
        .get()
        .open_bis_storage(EnumAsPrimitiveType::from(BisPartitionId::System))?;

    let fatfs_wrapper = FatFsWrapper::new(fs)?;

    let fatfs = match fatfs::FileSystem::new(fatfs_wrapper, fatfs::FsOptions::new()) {
        Ok(fatfs) => fatfs,
        Err(why) => {
            tx.tx(format!("Failed to open fatfs: {}", why));

            return Ok(None);
        }
    };

    let mut save_file = fatfs.root_dir().open_file("save/80000000000000E1")?;
    let mut buffer = alloc::vec![0u8; 0x400];
    for _ in 0..10_000 {
        let read_count = save_file.read(&mut buffer)?;

        if read_count == 0 {
            break;
        }

        let sig_type = u32::from_le_bytes([buffer[0], buffer[1], buffer[2], buffer[3]]);

        let sig_size = match sig_type {
            0x010000 => 0x200 + 0x3C,
            0x010003 => 0x200 + 0x3C,
            0x010001 => 0x100 + 0x3C,
            0x010004 => 0x100 + 0x3C,
            0x010002 => 0x3C + 0x40,
            0x010005 => 0x3C + 0x40,
            _ => continue,
        };

        let rights_id_offset = sig_size + 0x4 + 0x160;

        let rights_id = {
            let mut rid_buffer = [0u8; 0x10];
            rid_buffer.copy_from_slice(&buffer[rights_id_offset..(rights_id_offset + 0x10)]);

            RightsId::new(rid_buffer)
        };

        let ticket_title_id = rights_id.title_id();

        if ticket_title_id == program_id {
            let total_ticket_size = sig_size + 0x4 + 0x180;

            buffer.resize(total_ticket_size, 0);

            return Ok(Some((rights_id, buffer)));
        }
    }

    Ok(None)
}

pub fn fetch_common_certificate(rights_id: RightsId) -> Result<Vec<u8>> {
    let (c_tik_size, c_cert_size) = get_es_session()?
        .get()
        .get_common_ticket_and_certificate_size(rights_id)?;

    let mut ticket_buffer = alloc::vec![0u8; c_tik_size as usize];
    let mut cert_buffer = alloc::vec![0u8; c_cert_size as usize];
    get_es_session()?
        .get()
        .get_common_ticket_and_certificate_data(
            rights_id,
            Buffer::from_mut_array(&mut ticket_buffer),
            Buffer::from_mut_array(&mut cert_buffer),
        )?;

    Ok(cert_buffer)
}
