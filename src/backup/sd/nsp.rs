use alloc::{format, string::String};
use nx::{fs::FileOpenOption, ipc::sf::Buffer};

use crate::{
    backup::{
        es::{self, fetch_common_certificate},
        fetch_content_info_list, BackupResult,
    },
    hos::{
        fs::nsp::Pfs0Builder,
        hipc::ncm::{get_ncm_session, IContentStorage, ProgramId, StorageId},
        ncm::NcmContentType,
    },
    ui::logger_task::LoggerTx,
};

pub fn backup_nsp_from_console_to_sd(
    tx: &LoggerTx,
    name: String,
    program_id: ProgramId,
    storage_id: StorageId,
) -> BackupResult<()> {
    let out_nsp_path = format!(
        "sdmc:/meow/installer/{} [{:016X}][Meow!].nsp",
        name, program_id.0
    );
    tx.tx(format!(
        "Backing up from {} to {}",
        storage_id, out_nsp_path
    ));

    if nx::fs::get_entry_type(out_nsp_path.clone()).is_ok() {
        // tx.tx(format!("NSP \"{}\" already exists\nExiting...", out_nsp_path));
        let _ = nx::fs::delete_file(out_nsp_path.clone()); // This will error if the file doesn't exist and we can just ignore that
                                                           // return Ok(());
    }

    let mut pfs0_builder = Pfs0Builder::new();

    // Backup ticket
    let (rights_id, ticket) = match es::fetch_ticket(tx, program_id)? {
        Some(tuple) => tuple,
        None => {
            tx.tx("Missing ticket, exiting...");

            return Ok(());
        }
    };

    let ticket_name = format!(
        "{:016x}{:016x}.tik",
        rights_id.title_id().0,
        rights_id.keygen()
    );
    pfs0_builder.add_file(ticket_name.clone(), None, ticket.len() as u64);

    // Backup common certificate
    let cert_name = format!(
        "{:016x}{:016x}.cert",
        rights_id.title_id().0,
        rights_id.keygen()
    );
    let common_cert = fetch_common_certificate(rights_id)?;
    pfs0_builder.add_file(cert_name.clone(), None, common_cert.len() as u64);

    let content_infos = match fetch_content_info_list(storage_id, program_id)? {
        Some(list) => list,
        None => {
            tx.tx("Cannot find ContentMetaKey, exiting...");
            return Ok(());
        }
    };

    let content_storage = get_ncm_session()?.get().open_content_storage(storage_id)?;

    let mut nca_count = 0;

    for content_info in &content_infos {
        let file_name = match content_info.content_type {
            NcmContentType::Meta => {
                format!("{}.cnmt.nca", hex::encode(content_info.content_id.buffer))
            }
            _ => format!("{}.nca", hex::encode(content_info.content_id.buffer)),
        };
        pfs0_builder.add_file(
            file_name.clone(),
            Some((content_info.content_id, content_info.content_type)),
            content_storage
                .get()
                .get_size_from_content_id(content_info.content_id)?,
        );

        nca_count += 1;
    }

    let mut pfs0_offsets = pfs0_builder.create(out_nsp_path.clone())?;

    let mut file = nx::fs::open_file(out_nsp_path, FileOpenOption::Write())?;

    let ticket_offset = match pfs0_offsets.remove(&ticket_name) {
        Some((_, offset)) => offset,
        None => {
            tx.tx("PFS0 missing tiket entry");
            return Ok(());
        }
    };

    let cert_offset = match pfs0_offsets.remove(&cert_name) {
        Some((_, offset)) => offset,
        None => {
            tx.tx("PFS0 missing certificate entry");
            return Ok(());
        }
    };

    file.seek(ticket_offset, nx::fs::Whence::Start)?;
    file.write_array(&ticket)?;
    tx.tx(format!(
        "Written ticket to {} ({} bytes)",
        ticket_name,
        ticket.len()
    ));

    file.seek(cert_offset, nx::fs::Whence::Start)?;
    file.write_array(&common_cert)?;
    tx.tx(format!(
        "Written certificate to {} ({} bytes)",
        cert_name,
        common_cert.len()
    ));

    for (name, (content_info, offset)) in pfs0_offsets {
        let (content_id, content_type) = match content_info {
            Some(tuple) => tuple,
            None => {
                tx.tx("NCA missing content id...");
                return Ok(());
            }
        };

        file.seek(offset, nx::fs::Whence::Start)?;

        let total_size = content_storage.get().get_size_from_content_id(content_id)? as usize;
        let mut remaining_size = total_size;
        let mut offset = 0u64;
        let mut vec = alloc::vec![0u8; 64_000_000];
        let threshold_offset = total_size / 40;
        let mut next_threshold = threshold_offset;

        tx.tx(format!(
            "Writing {} ({}) - {}% {}/{}",
            name, content_type, 0, 0, total_size
        ));

        while remaining_size != 0 {
            let iter_size = remaining_size.min(64_000_000);

            let write_buffer = &mut vec[..iter_size];

            content_storage.get().read_content_id_file(
                Buffer::from_mut_array(write_buffer),
                content_id,
                offset as i64,
            )?;
            file.write_array(write_buffer)?;

            offset += iter_size as u64;
            remaining_size -= iter_size;

            if offset >= next_threshold as u64 {
                let percent = ((offset * 100) / total_size as u64).min(100).max(0);
                tx.edit_last(format!(
                    "Writing {} ({}) - {}% {}/{}",
                    name, content_type, percent, offset, total_size
                ));

                while offset >= next_threshold as u64 {
                    next_threshold += threshold_offset;
                }
            }
        }
    }

    tx.tx(format!(
        "Done, built an NSP containing {} NCAs, 1 ticket, and 1 certificate!",
        nca_count
    ));

    Ok(())
}
