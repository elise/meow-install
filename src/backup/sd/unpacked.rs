use alloc::{format, string::String};
use nx::{fs::FileOpenOption, ipc::sf::Buffer};

use crate::{
    backup::{
        es::{self, fetch_common_certificate},
        fetch_content_info_list, BackupResult,
    },
    hos::{
        hipc::ncm::{get_ncm_session, IContentStorage, ProgramId, StorageId},
        ncm::NcmContentType,
    },
    ui::logger_task::LoggerTx,
};

pub fn backup_unpacked_nsp_from_console_to_sd(
    tx: &LoggerTx,
    name: String,
    program_id: ProgramId,
    storage_id: StorageId,
) -> BackupResult<()> {
    let out_dir_path = format!(
        "sdmc:/meow/installer/{} [{:016X}][Meow!]",
        name, program_id.0
    );

    if nx::fs::get_entry_type(out_dir_path.clone()).is_ok() {
        tx.tx(format!(
            "Path \"{}\" already exists\nExiting...",
            out_dir_path
        ));
        return Ok(());
    } else {
        nx::fs::create_directory(out_dir_path.clone())?;
    }

    tx.tx(format!(
        "Backing up from {} to {}",
        storage_id, out_dir_path
    ));

    let rights_id;

    // Backup ticket
    {
        let (local_rights_id, ticket) = match es::fetch_ticket(tx, program_id)? {
            Some(tuple) => tuple,
            None => {
                tx.tx("Missing ticket, exiting...");

                return Ok(());
            }
        };

        rights_id = local_rights_id;

        let ticket_path = format!(
            "{}/{:016x}{:016x}.tik",
            out_dir_path,
            rights_id.title_id().0,
            rights_id.keygen()
        );
        let _ = nx::fs::delete_file(ticket_path.clone()); // This will error if the file doesn't exist and we can just ignore that
        let mut ticket_file = nx::fs::open_file(
            ticket_path.clone(),
            FileOpenOption::Create() | FileOpenOption::Write() | FileOpenOption::Append(),
        )?;
        ticket_file.write_array(&ticket)?;
        tx.tx(format!(
            "Written ticket to {} ({} bytes)",
            ticket_path.split('/').last().unwrap_or_default(),
            ticket.len()
        ));
    }

    // Backup common certificate
    {
        let cert_path = format!(
            "{}/{:016x}{:016x}.cert",
            out_dir_path,
            rights_id.title_id().0,
            rights_id.keygen()
        );
        let common_cert = fetch_common_certificate(rights_id)?;
        let _ = nx::fs::delete_file(cert_path.clone()); // This will error if the file doesn't exist and we can just ignore that
        let mut cert_file = nx::fs::open_file(
            cert_path.clone(),
            FileOpenOption::Create() | FileOpenOption::Write() | FileOpenOption::Append(),
        )?;
        cert_file.write_array(&common_cert)?;
        tx.tx(format!(
            "Written certificate to {} ({} bytes)",
            cert_path.split('/').last().unwrap_or_default(),
            common_cert.len()
        ));
    }

    let content_infos = match fetch_content_info_list(storage_id, program_id)? {
        Some(list) => list,
        None => {
            tx.tx("Cannot find ContentMetaKey, exiting...");
            return Ok(());
        }
    };

    // Backup NCAs
    let content_storage = get_ncm_session()?.get().open_content_storage(storage_id)?;

    let mut nca_count = 0;

    for content_info in content_infos {
        let nca_id = content_info.content_id;

        let nca_file_name = match content_info.content_type {
            NcmContentType::Meta => format!("{}.cnmt.nca", hex::encode(nca_id.buffer)),
            _ => format!("{}.nca", hex::encode(nca_id.buffer)),
        };

        let nca_size = content_storage.get().get_size_from_content_id(nca_id)?;

        let nca_path = format!("{}/{}", out_dir_path, nca_file_name);
        let _ = nx::fs::delete_file(nca_path.clone()); // This will error if the file doesn't exist and we can just ignore that
        let mut nca_file = nx::fs::open_file(
            nca_path,
            FileOpenOption::Create() | FileOpenOption::Write() | FileOpenOption::Append(),
        )?;

        let mut remaining_size = nca_size as usize;
        let mut offset = 0u64;
        let mut vec = alloc::vec![0u8; 64_000_000];
        let threshold_offset = nca_size / 40;
        let mut next_threshold = threshold_offset;

        tx.tx(format!(
            "Writing {} ({}) - {}% {}/{}",
            name, content_info.content_type, 0, 0, nca_size
        ));
        while remaining_size != 0 {
            let iter_size = remaining_size.min(64_000_000);

            let write_buffer = &mut vec[..iter_size];

            content_storage.get().read_content_id_file(
                Buffer::from_mut_array(write_buffer),
                nca_id,
                offset as i64,
            )?;
            nca_file.write_array(write_buffer)?;

            offset += iter_size as u64;
            remaining_size -= iter_size;

            if offset >= next_threshold {
                let percent = ((offset * 100) / nca_size).min(100).max(0);
                tx.edit_last(format!(
                    "Writing {} ({}) - {}% {}/{}",
                    name, content_info.content_type, percent, offset, nca_size
                ));

                while offset >= next_threshold {
                    next_threshold += threshold_offset;
                }
            }
        }

        nca_count += 1;
    }

    tx.tx(format!(
        "Done, written {} NCAs, 1 ticket, and 1 certificate!",
        nca_count
    ));

    Ok(())
}
