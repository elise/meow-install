use alloc::format;

use crate::ui::logger_task::LoggerTx;

use self::hipc::ncm::ContentMetaType;

pub mod fs;
pub mod hipc;
pub mod ncm;

pub fn parse_nca_id(tx: &LoggerTx, input: &str) -> Option<[u8; 0x10]> {
    if input.len() != 0x20 {
        tx.tx(format!(
            "NCA ID input is only {} bytes long, expected 32",
            input.len()
        ));
        return None;
    }

    let mut out = [0u8; 0x10];

    if let Err(why) = hex::decode_to_slice(input, &mut out) {
        tx.tx(format!("Failed to parse NCA ID: {}", why));
        return None;
    }

    Some(out)
}

pub fn title_id_to_base(title_id: u64, content_type: ContentMetaType) -> u64 {
    match content_type {
        ContentMetaType::Patch => title_id ^ 0x800,
        ContentMetaType::AddOnContent => (title_id ^ 0x1000) & !0xFFF,
        _ => title_id,
    }
}
