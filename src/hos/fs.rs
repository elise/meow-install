use nx::{
    ipc::sf::{Buffer, EnumAsPrimitiveType},
    mem::Shared,
    result::Result,
    service::fsp::{ContentAttributes, FileSystemProxyType, IFileSystem, Path, RightsId},
    version,
};

pub mod nand;
pub mod nca;
pub mod nsp;

pub fn get_rights_id_and_key_generation_by_path(path: &str) -> Result<(u64, RightsId)> {
    if version::get_version() < version::Version::new(16, 0, 0) {
        nx::fs::get_fspsrv_session()?
            .get()
            .get_rights_id_and_key_generation_by_path_pre_16_0_0(Buffer::from_var(&Path::from_str(
                path,
            )))
    } else {
        nx::fs::get_fspsrv_session()?
            .get()
            .get_rights_id_and_key_generation_by_path_post_16_0_0(
                Buffer::from_var(&Path::from_str(path)),
                EnumAsPrimitiveType::from(ContentAttributes::All),
            )
    }
}

pub fn open_filesystem_with_id(
    path: Path,
    _attr: ContentAttributes,
    proxy_type: FileSystemProxyType,
    title_id: u64,
) -> Result<Shared<dyn IFileSystem>> {
    return nx::fs::get_fspsrv_session()?
        .get()
        .open_filesystem_with_id_pre_16_0_0(Buffer::from_var(&path), proxy_type, title_id);
    // if version::get_version() < version::Version::new(16, 0, 0) {
    //     nx::fs::get_fspsrv_session()?.get().open_filesystem_with_id_pre_16_0_0(Buffer::from_var(&path), proxy_type, title_id)
    // } else {
    //     nx::fs::get_fspsrv_session()?.get().open_filesystem_with_id_post_16_0_0(Buffer::from_var(&path), ContentAttributes::All, proxy_type, title_id)
    // }
}
