use core::sync::atomic::AtomicUsize;

use alloc::{format, string::String};
use nx::{
    fs::{FileSystem, ProxyFileSystem},
    ipc::sf::{Buffer, EnumAsPrimitiveType},
    mem::Shared,
    result::Result,
    service::fsp::{BisPartitionId, Path},
};

static CTR: AtomicUsize = AtomicUsize::new(0);

pub struct NandFileSystem {
    filesystem: Shared<ProxyFileSystem>,
    mount: Option<String>,
}

impl NandFileSystem {
    pub fn new(partition: BisPartitionId) -> Result<Self> {
        let path_buf = Path::from_str("");
        let fs = nx::fs::get_fspsrv_session()?.get().open_bis_filesystem(
            Buffer::from_var(&path_buf),
            EnumAsPrimitiveType::from(partition),
        )?;
        let proxy_fs = ProxyFileSystem::new(fs);
        Ok(Self {
            filesystem: Shared::new(proxy_fs),
            mount: None,
        })
    }

    pub fn mount(&mut self) -> Result<String> {
        if let Some(mountpoint) = self.mount.as_ref() {
            Ok(mountpoint.clone())
        } else {
            let string_val = format!(
                "meow-{}",
                CTR.fetch_add(1, core::sync::atomic::Ordering::SeqCst)
            );
            nx::fs::mount(&string_val, self.filesystem.clone())?;
            Ok(string_val)
        }
    }

    pub fn unmount(&mut self) {
        if let Some(mountpoint) = self.mount.as_ref() {
            nx::fs::unmount(mountpoint);
        }
    }
}

impl Drop for NandFileSystem {
    fn drop(&mut self) {
        self.unmount();
    }
}

impl FileSystem for NandFileSystem {
    fn create_file(
        &mut self,
        path: alloc::string::String,
        attribute: nx::fs::FileAttribute,
        size: usize,
    ) -> Result<()> {
        self.filesystem.get().create_file(path, attribute, size)
    }

    fn delete_file(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.get().delete_file(path)
    }

    fn create_directory(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.get().create_directory(path)
    }

    fn delete_directory(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.get().delete_directory(path)
    }

    fn delete_directory_recursively(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.get().delete_directory_recursively(path)
    }

    fn rename_file(
        &mut self,
        old_path: alloc::string::String,
        new_path: alloc::string::String,
    ) -> Result<()> {
        self.filesystem.get().rename_file(old_path, new_path)
    }

    fn rename_directory(
        &mut self,
        old_path: alloc::string::String,
        new_path: alloc::string::String,
    ) -> Result<()> {
        self.filesystem.get().rename_directory(old_path, new_path)
    }

    fn get_entry_type(
        &mut self,
        path: alloc::string::String,
    ) -> Result<nx::fs::DirectoryEntryType> {
        self.filesystem.get().get_entry_type(path)
    }

    fn open_file(
        &mut self,
        path: alloc::string::String,
        mode: nx::fs::FileOpenMode,
    ) -> Result<nx::mem::Shared<dyn nx::fs::File>> {
        self.filesystem.get().open_file(path, mode)
    }

    fn open_directory(
        &mut self,
        path: alloc::string::String,
        mode: nx::fs::DirectoryOpenMode,
    ) -> Result<nx::mem::Shared<dyn nx::fs::Directory>> {
        self.filesystem.get().open_directory(path, mode)
    }

    fn commit(&mut self) -> Result<()> {
        self.filesystem.get().commit()
    }

    fn get_free_space_size(&mut self, path: alloc::string::String) -> Result<usize> {
        self.filesystem.get().get_free_space_size(path)
    }

    fn get_total_space_size(&mut self, path: alloc::string::String) -> Result<usize> {
        self.filesystem.get().get_total_space_size(path)
    }

    fn clean_directory_recursively(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.get().clean_directory_recursively(path)
    }

    fn get_file_time_stamp_raw(
        &mut self,
        path: alloc::string::String,
    ) -> Result<nx::fs::FileTimeStampRaw> {
        self.filesystem.get().get_file_time_stamp_raw(path)
    }

    fn query_entry(
        &mut self,
        path: alloc::string::String,
        query_id: nx::fs::QueryId,
        in_buf: *const u8,
        in_buf_size: usize,
        out_buf: *mut u8,
        out_buf_size: usize,
    ) -> Result<()> {
        self.filesystem.get().query_entry(
            path,
            query_id,
            in_buf,
            in_buf_size,
            out_buf,
            out_buf_size,
        )
    }
}
