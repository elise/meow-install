use alloc::{string::String, vec::Vec};
use nx::{
    fs::{DirectoryEntry, DirectoryOpenMode, FileSystem, ProxyFileSystem},
    result::Result,
    service::fsp::{ContentAttributes, Path},
};

use super::open_filesystem_with_id;

pub struct NcaFileSystem {
    filesystem: ProxyFileSystem,
}

impl NcaFileSystem {
    pub fn new(
        path: String,
        proxy_type: nx::service::fsp::FileSystemProxyType,
        title_id: u64,
    ) -> Result<Self> {
        let path_buf = Path::from_string(path);
        let fs = {
            let mut res =
                open_filesystem_with_id(path_buf, ContentAttributes::All, proxy_type, title_id);

            if res.is_err() {
                res =
                    open_filesystem_with_id(path_buf, ContentAttributes::All, proxy_type, title_id)
            }

            res?
        };
        let proxy_fs = ProxyFileSystem::new(fs);
        Ok(Self {
            filesystem: proxy_fs,
        })
    }

    pub fn get_file_by_ext(&mut self, ext: &str) -> Result<Option<String>> {
        let dir = self.open_directory(String::from("/"), DirectoryOpenMode::ReadFiles())?;

        let mut remaining_entry_count = dir.get().get_entry_count()?;

        while remaining_entry_count > 0 {
            let read_count = remaining_entry_count.min(16);
            let mut holder = Vec::new();
            holder.resize(read_count as usize, DirectoryEntry::default());

            dir.get().read(&mut holder)?;

            for entry in holder {
                let name = entry.name.get_string()?;
                if name.ends_with(ext) {
                    return Ok(Some(name));
                }
            }

            remaining_entry_count -= read_count;
        }

        Ok(None)
    }
}

impl FileSystem for NcaFileSystem {
    fn create_file(
        &mut self,
        path: alloc::string::String,
        attribute: nx::fs::FileAttribute,
        size: usize,
    ) -> Result<()> {
        self.filesystem.create_file(path, attribute, size)
    }

    fn delete_file(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.delete_file(path)
    }

    fn create_directory(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.create_directory(path)
    }

    fn delete_directory(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.delete_directory(path)
    }

    fn delete_directory_recursively(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.delete_directory_recursively(path)
    }

    fn rename_file(
        &mut self,
        old_path: alloc::string::String,
        new_path: alloc::string::String,
    ) -> Result<()> {
        self.filesystem.rename_file(old_path, new_path)
    }

    fn rename_directory(
        &mut self,
        old_path: alloc::string::String,
        new_path: alloc::string::String,
    ) -> Result<()> {
        self.filesystem.rename_directory(old_path, new_path)
    }

    fn get_entry_type(
        &mut self,
        path: alloc::string::String,
    ) -> Result<nx::fs::DirectoryEntryType> {
        self.filesystem.get_entry_type(path)
    }

    fn open_file(
        &mut self,
        path: alloc::string::String,
        mode: nx::fs::FileOpenMode,
    ) -> Result<nx::mem::Shared<dyn nx::fs::File>> {
        self.filesystem.open_file(path, mode)
    }

    fn open_directory(
        &mut self,
        path: alloc::string::String,
        mode: nx::fs::DirectoryOpenMode,
    ) -> Result<nx::mem::Shared<dyn nx::fs::Directory>> {
        self.filesystem.open_directory(path, mode)
    }

    fn commit(&mut self) -> Result<()> {
        self.filesystem.commit()
    }

    fn get_free_space_size(&mut self, path: alloc::string::String) -> Result<usize> {
        self.filesystem.get_free_space_size(path)
    }

    fn get_total_space_size(&mut self, path: alloc::string::String) -> Result<usize> {
        self.filesystem.get_total_space_size(path)
    }

    fn clean_directory_recursively(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.clean_directory_recursively(path)
    }

    fn get_file_time_stamp_raw(
        &mut self,
        path: alloc::string::String,
    ) -> Result<nx::fs::FileTimeStampRaw> {
        self.filesystem.get_file_time_stamp_raw(path)
    }

    fn query_entry(
        &mut self,
        path: alloc::string::String,
        query_id: nx::fs::QueryId,
        in_buf: *const u8,
        in_buf_size: usize,
        out_buf: *mut u8,
        out_buf_size: usize,
    ) -> Result<()> {
        self.filesystem
            .query_entry(path, query_id, in_buf, in_buf_size, out_buf, out_buf_size)
    }
}
