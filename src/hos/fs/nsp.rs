use alloc::{collections::BTreeMap, format, string::String, vec::Vec};
use nx::{
    fs::{
        Directory, DirectoryEntry, DirectoryOpenMode, FileAttribute, FileOpenMode, FileOpenOption,
        FileReadOption, FileSystem, ProxyFileSystem,
    },
    ipc::sf::{Buffer, InMapAliasBuffer},
    mem::Shared,
    result::Result,
    service::fsp::{ContentAttributes, Path},
};

use crate::{
    hos::{
        hipc::ncm::{ContentId, ContentStorage, IContentStorage, PlaceholderId},
        ncm::NcmContentType,
    },
    ui::logger_task::LoggerTx,
};

use super::open_filesystem_with_id;

#[derive(Debug)]
pub struct Pfs0Builder {
    string_table_size: u32,

    #[allow(clippy::type_complexity)]
    partitions: Vec<(String, Option<(ContentId, NcmContentType)>, u64)>,
}

impl Default for Pfs0Builder {
    fn default() -> Self {
        Self::new()
    }
}

impl Pfs0Builder {
    pub fn new() -> Self {
        Self {
            string_table_size: 0,
            partitions: Vec::new(),
        }
    }

    pub fn add_file(
        &mut self,
        name: String,
        content_info: Option<(ContentId, NcmContentType)>,
        size: u64,
    ) {
        let string_table_entry_len = name.as_bytes().len() + 1;
        self.string_table_size += string_table_entry_len as u32;

        self.partitions.push((name, content_info, size));
    }

    #[allow(clippy::type_complexity)]
    pub fn create(
        self,
        path: String,
    ) -> Result<BTreeMap<String, (Option<(ContentId, NcmContentType)>, usize)>> {
        let total_size = core::mem::size_of::<Pfs0ContainerHeader>()
            + self.string_table_size as usize
            + (core::mem::size_of::<Pfs0ContainerPartitionEntry>() * self.partitions.len())
            + self.partitions.iter().fold(0, |x, (_, _, size)| x + *size) as usize;
        nx::fs::create_file(path.clone(), total_size, FileAttribute::None())?;

        let mut file_offset = 0;
        let mut file = nx::fs::open_file(path, FileOpenOption::Write())?;

        file.write_val(&Pfs0ContainerHeader::new(
            self.partitions.len() as u32,
            self.string_table_size,
        ))?;
        file_offset += core::mem::size_of::<Pfs0ContainerHeader>();

        let mut str_table_offset = 0;
        let mut data_offset = 0;
        let mut out = BTreeMap::new();

        for (name, _content_id, size) in &self.partitions {
            file.write_val(&Pfs0ContainerPartitionEntry::new(
                data_offset,
                *size,
                str_table_offset,
            ))?;
            file_offset += core::mem::size_of::<Pfs0ContainerPartitionEntry>();
            str_table_offset += name.as_bytes().len() as u32 + 1;
            data_offset += *size
        }

        for (name, _content_id, _size) in &self.partitions {
            file.write_array(name.as_bytes())?;
            file.write_val(&0u8)?;
            file_offset += name.as_bytes().len() + 1;
        }

        for (name, content_info, size) in &self.partitions {
            out.insert(name.clone(), (*content_info, file_offset));
            file.seek(*size as usize, nx::fs::Whence::Current)?;
            file_offset += *size as usize;
        }

        Ok(out)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)]
pub struct Pfs0ContainerPartitionEntry {
    pub offset: u64,
    pub size: u64,
    pub string_table_offset: u32,
    _res_14: u32,
}

impl Pfs0ContainerPartitionEntry {
    pub fn new(offset: u64, size: u64, string_table_offset: u32) -> Self {
        Self {
            offset,
            size,
            string_table_offset,
            _res_14: 0,
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)]
pub struct Pfs0ContainerHeader {
    pub magic: [u8; 4],
    pub entry_count: u32,
    pub string_table_size: u32,
    pub _res_c: u32,
}

impl Default for Pfs0ContainerHeader {
    fn default() -> Self {
        Self {
            magic: *b"PFS0",
            entry_count: 0,
            string_table_size: 0,
            _res_c: 0,
        }
    }
}

impl Pfs0ContainerHeader {
    pub fn new(entry_count: u32, string_table_size: u32) -> Self {
        Self {
            magic: *b"PFS0",
            entry_count,
            string_table_size,
            _res_c: 0,
        }
    }
}

pub struct NspFileSystem {
    filesystem: ProxyFileSystem,
}

impl NspFileSystem {
    pub fn create(sd_path: String) -> Result<Self> {
        let fs_obj = open_filesystem_with_id(
            Path::from_string(format!("@Sdcard{}", sd_path.trim_start_matches("sdmc"))),
            ContentAttributes::All,
            nx::service::fsp::FileSystemProxyType::Package,
            0,
        )?;

        Ok(Self {
            filesystem: ProxyFileSystem::new(fs_obj),
        })
    }

    pub fn mount(path: String) -> Result<Self> {
        let fs_obj = open_filesystem_with_id(
            Path::from_string(path),
            ContentAttributes::All,
            nx::service::fsp::FileSystemProxyType::Package,
            0,
        )?;
        Ok(Self {
            filesystem: ProxyFileSystem::new(fs_obj),
        })
    }

    pub fn get_cnmt_name(&mut self) -> Result<Option<String>> {
        let dir = self.open_directory(String::from("/"), DirectoryOpenMode::ReadFiles())?;

        let mut remaining_entry_count = dir.get().get_entry_count()?;

        while remaining_entry_count > 0 {
            let read_count = remaining_entry_count.min(16);
            let mut holder = Vec::new();
            holder.resize(read_count as usize, DirectoryEntry::default());

            dir.get().read(&mut holder)?;

            for entry in holder {
                let name = entry.name.get_string()?;
                if name.ends_with(".cnmt.nca") {
                    return Ok(Some(name));
                }
            }

            remaining_entry_count -= read_count;
        }

        Ok(None)
    }

    pub fn get_files_by_ext(&mut self, ext: &str) -> Result<Vec<String>> {
        let dir = self.open_directory(String::from("/"), DirectoryOpenMode::ReadFiles())?;

        let mut out = Vec::new();

        let mut remaining_entry_count = dir.get().get_entry_count()?;

        while remaining_entry_count > 0 {
            let read_count = remaining_entry_count.min(16);
            let mut holder = Vec::new();
            holder.resize(read_count as usize, DirectoryEntry::default());

            dir.get().read(&mut holder)?;

            for entry in holder {
                let name = entry.name.get_string()?;
                if name.ends_with(ext) {
                    out.push(name);
                }
            }

            remaining_entry_count -= read_count;
        }

        Ok(out)
    }

    pub fn get_nca_path_by_id(&mut self, nca_id: [u8; 0x10]) -> Result<Option<String>> {
        let dir: Shared<dyn Directory> =
            self.open_directory(String::from("/"), DirectoryOpenMode::ReadFiles())?;

        let mut remaining_entry_count = dir.get().get_entry_count()?;

        while remaining_entry_count > 0 {
            let read_count = remaining_entry_count.min(16);
            let mut holder = Vec::new();
            holder.resize(read_count as usize, DirectoryEntry::default());

            dir.get().read(&mut holder)?;

            for entry in holder {
                let name = entry.name.get_string()?;
                if name.ends_with(".nca") {
                    let file_nca_id = name.trim_end_matches(".nca").trim_end_matches(".cnmt");
                    if let Ok(file_nca_id) = hex::decode(file_nca_id) {
                        if nca_id == file_nca_id.as_slice() {
                            return Ok(Some(format!("/{}", name)));
                        }
                    }
                }
            }

            remaining_entry_count -= read_count;
        }

        Ok(None)
    }

    pub fn get_ncas(&mut self) -> Result<Vec<String>> {
        self.get_files_by_ext(".nca")
    }

    pub fn copy_file(&mut self, in_path: String, out_path: String) -> Result<()> {
        let in_file = self.open_file(in_path, FileOpenMode::Read())?;
        let mut remaining_size = in_file.get().get_size()?;

        let mut out_file =
            nx::fs::open_file(out_path, FileOpenOption::Write() | FileOpenOption::Create())?;

        let mut offset = 0;

        while remaining_size != 0 {
            let iter_size = remaining_size.min(4000);

            let mut vec = Vec::with_capacity(iter_size);
            vec.fill(0u8);

            assert_eq!(
                in_file
                    .get()
                    .read(offset, vec.as_mut_ptr(), iter_size, FileReadOption::None())?,
                iter_size
            );
            out_file.write_array(&vec)?;

            offset += iter_size;
            remaining_size -= iter_size;
        }

        Ok(())
    }

    pub fn copy_registered_nca_to_nsp(
        &mut self,
        tx: &LoggerTx,
        nca_id: ContentId,
        content_type: NcmContentType,
        content_storage: Shared<ContentStorage>,
    ) -> Result<Option<()>> {
        let nca_path = match content_type {
            NcmContentType::Meta => format!("/{}.cnmt.nca", hex::encode(nca_id.buffer)),
            _ => format!("/{}.nca", hex::encode(nca_id.buffer)),
        };
        self.create_file(nca_path.clone(), FileAttribute::None(), 0)?;

        let nca_size = content_storage.get().get_size_from_content_id(nca_id)?;

        tx.tx(format!(
            "Backing up {} ({} bytes, {})",
            nca_path.as_str().trim_start_matches('/'),
            nca_size,
            content_type
        ));

        let _ = nx::fs::delete_file(nca_path.clone()); // This will error if the file doesn't exist and we can just ignore that
        let mut nca_file = nx::fs::open_file(
            nca_path,
            FileOpenOption::Create() | FileOpenOption::Write() | FileOpenOption::Append(),
        )?;

        let mut remaining_size = nca_size as usize;
        let mut offset = 0u64;
        let mut vec = alloc::vec![0u8; 64_000_000];

        while remaining_size != 0 {
            let iter_size = remaining_size.min(64_000_000);

            let write_buffer = &mut vec[..iter_size];

            content_storage.get().read_content_id_file(
                Buffer::from_mut_array(write_buffer),
                nca_id,
                offset as i64,
            )?;
            nca_file.write_array(write_buffer)?;

            offset += iter_size as u64;
            remaining_size -= iter_size;
        }

        Ok(Some(()))
    }

    pub fn copy_nca_to_placeholder(
        &mut self,
        tx: &LoggerTx,
        nca_id: [u8; 0x10],
        content_storage: Shared<ContentStorage>,
    ) -> Result<Option<()>> {
        let nca_path = match self.get_nca_path_by_id(nca_id)? {
            Some(path) => path,
            None => return Ok(None),
        };
        let nca_file = self.open_file(nca_path.clone(), FileOpenMode::Read())?;

        let total_size = nca_file.get().get_size()?;
        let mut remaining_size = total_size;
        let mut offset = 0u64;
        let mut vec = alloc::vec![0u8; 64_000_000];
        let threshold_offset = total_size / 40;
        let mut next_threshold = threshold_offset;

        tx.tx(format!(
            "Installing {} - {}% {}/{}",
            nca_path.as_str().trim_start_matches('/'),
            0,
            0,
            total_size
        ));

        while remaining_size != 0 {
            let iter_size = remaining_size.min(64_000_000);

            let write_buffer = &mut vec[..iter_size];

            nca_file.get().read(
                offset as usize,
                write_buffer.as_mut_ptr(),
                iter_size,
                FileReadOption::None(),
            )?;
            content_storage.get().write_placeholder(
                PlaceholderId::new(nca_id),
                offset,
                InMapAliasBuffer::from_mut_array(write_buffer),
            )?;

            offset += iter_size as u64;
            remaining_size -= iter_size;

            if offset >= next_threshold as u64 {
                let percent = ((offset * 100) / total_size as u64).min(100).max(0);
                tx.edit_last(format!(
                    "Installing {} - {}% {}/{}",
                    nca_path.as_str().trim_start_matches('/'),
                    percent,
                    offset,
                    total_size
                ));

                while offset >= next_threshold as u64 {
                    next_threshold += threshold_offset;
                }
            }
        }

        Ok(Some(()))
    }
}

impl FileSystem for NspFileSystem {
    fn create_file(
        &mut self,
        path: alloc::string::String,
        attribute: nx::fs::FileAttribute,
        size: usize,
    ) -> Result<()> {
        self.filesystem.create_file(path, attribute, size)
    }

    fn delete_file(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.delete_file(path)
    }

    fn create_directory(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.create_directory(path)
    }

    fn delete_directory(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.delete_directory(path)
    }

    fn delete_directory_recursively(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.delete_directory_recursively(path)
    }

    fn rename_file(
        &mut self,
        old_path: alloc::string::String,
        new_path: alloc::string::String,
    ) -> Result<()> {
        self.filesystem.rename_file(old_path, new_path)
    }

    fn rename_directory(
        &mut self,
        old_path: alloc::string::String,
        new_path: alloc::string::String,
    ) -> Result<()> {
        self.filesystem.rename_directory(old_path, new_path)
    }

    fn get_entry_type(
        &mut self,
        path: alloc::string::String,
    ) -> Result<nx::fs::DirectoryEntryType> {
        self.filesystem.get_entry_type(path)
    }

    fn open_file(
        &mut self,
        path: alloc::string::String,
        mode: nx::fs::FileOpenMode,
    ) -> Result<nx::mem::Shared<dyn nx::fs::File>> {
        self.filesystem.open_file(path, mode)
    }

    fn open_directory(
        &mut self,
        path: alloc::string::String,
        mode: nx::fs::DirectoryOpenMode,
    ) -> Result<nx::mem::Shared<dyn nx::fs::Directory>> {
        self.filesystem.open_directory(path, mode)
    }

    fn commit(&mut self) -> Result<()> {
        self.filesystem.commit()
    }

    fn get_free_space_size(&mut self, path: alloc::string::String) -> Result<usize> {
        self.filesystem.get_free_space_size(path)
    }

    fn get_total_space_size(&mut self, path: alloc::string::String) -> Result<usize> {
        self.filesystem.get_total_space_size(path)
    }

    fn clean_directory_recursively(&mut self, path: alloc::string::String) -> Result<()> {
        self.filesystem.clean_directory_recursively(path)
    }

    fn get_file_time_stamp_raw(
        &mut self,
        path: alloc::string::String,
    ) -> Result<nx::fs::FileTimeStampRaw> {
        self.filesystem.get_file_time_stamp_raw(path)
    }

    fn query_entry(
        &mut self,
        path: alloc::string::String,
        query_id: nx::fs::QueryId,
        in_buf: *const u8,
        in_buf_size: usize,
        out_buf: *mut u8,
        out_buf_size: usize,
    ) -> Result<()> {
        self.filesystem
            .query_entry(path, query_id, in_buf, in_buf_size, out_buf, out_buf_size)
    }
}
