use alloc::vec;
use nx::{
    ipc::sf::{self, InMapAliasBuffer, OutMapAliasBuffer},
    ipc_client_define_object_default, ipc_client_send_request_command,
    ipc_sf_define_interface_trait,
    mem::Shared,
    rc::ResultNotInitialized,
    result::{Result, ResultBase},
    service::{sm::ServiceName, IService},
    version::VersionInterval,
};

use super::ncm::ProgramId;

#[derive(Copy, Clone, PartialEq, Eq, Debug, Default)]
#[repr(C)]
pub struct RightsId {
    pub buffer: [u8; 0x10],
}

impl RightsId {
    pub fn new(buffer: [u8; 0x10]) -> Self {
        Self { buffer }
    }

    pub fn title_id(self) -> ProgramId {
        ProgramId(u64::from_be_bytes([
            self.buffer[0],
            self.buffer[1],
            self.buffer[2],
            self.buffer[3],
            self.buffer[4],
            self.buffer[5],
            self.buffer[6],
            self.buffer[7],
        ]))
    }

    pub fn keygen(self) -> u64 {
        u64::from_be_bytes([
            self.buffer[8],
            self.buffer[9],
            self.buffer[10],
            self.buffer[11],
            self.buffer[12],
            self.buffer[13],
            self.buffer[14],
            self.buffer[15],
        ])
    }
}

ipc_sf_define_interface_trait! {
    trait IETicketService {
        import_ticket [1, VersionInterval::all()]: (ticket: InMapAliasBuffer<u8>, certificate: InMapAliasBuffer<u8>) => ();
        delete_ticket [3, VersionInterval::all()]: (rights_id: RightsId) => ();
        get_common_ticket_and_certificate_size [22, VersionInterval::all()]: (rights_id: RightsId) => (ticket_size: i64, certificate_size: i64);
        get_common_ticket_and_certificate_data [23, VersionInterval::all()]: (rights_id: RightsId, out_ticket: OutMapAliasBuffer<u8>, out_cert: OutMapAliasBuffer<u8>) => (ticket_size: i64, certificate_size: i64);
    }
}

ipc_client_define_object_default!(ETicketService);

impl IETicketService for ETicketService {
    #[allow(unused_parens)]
    fn import_ticket(
        &mut self,
        ticket: InMapAliasBuffer<u8>,
        certificate: InMapAliasBuffer<u8>,
    ) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 1] (ticket, certificate) => ())
    }

    #[allow(unused_parens)]
    fn delete_ticket(&mut self, rights_id: RightsId) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 3] (rights_id) => ())
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn get_common_ticket_and_certificate_size(
        &mut self,
        rights_id: RightsId,
    ) -> Result<(i64, i64)> {
        ipc_client_send_request_command!([self.session.object_info; 22] (rights_id) => (ticket_size: i64, cert_size: i64))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn get_common_ticket_and_certificate_data(
        &mut self,
        rights_id: RightsId,
        out_ticket: OutMapAliasBuffer<u8>,
        out_cert: OutMapAliasBuffer<u8>,
    ) -> Result<(i64, i64)> {
        ipc_client_send_request_command!([self.session.object_info; 23] (rights_id, out_ticket, out_cert) => (ticket_size: i64, cert_size: i64))
    }
}

impl IService for ETicketService {
    fn get_name() -> ServiceName {
        ServiceName::new("es")
    }

    fn as_domain() -> bool {
        false
    }

    fn post_initialize(&mut self) -> Result<()> {
        Ok(())
    }
}

static mut G_ETICKET_SESSION: nx::sync::Locked<Option<Shared<dyn IETicketService>>> =
    nx::sync::Locked::new(false, None);

pub fn initialise_es_session(session: Shared<ETicketService>) {
    unsafe {
        G_ETICKET_SESSION.set(Some(session));
    }
}

pub fn finalise_es_session() {
    unsafe {
        G_ETICKET_SESSION.set(None);
    }
}

#[inline]
pub fn get_es_session() -> Result<&'static Shared<dyn IETicketService>> {
    unsafe {
        G_ETICKET_SESSION
            .get()
            .as_ref()
            .ok_or(ResultNotInitialized::make())
    }
}
