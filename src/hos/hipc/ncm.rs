use core::fmt::{Debug, Display};

use alloc::{string::String, vec};
use nx::{
    ipc::sf::{
        self, Buffer, EnumAsPrimitiveType, InMapAliasBuffer, OutFixedPointerBuffer,
        OutMapAliasBuffer,
    },
    ipc_client_define_object_default, ipc_client_send_request_command,
    ipc_sf_define_interface_trait,
    mem::Shared,
    rc::ResultNotInitialized,
    result::{Result, ResultBase},
    service::{fsp::Path, sm::ServiceName, IService},
    version::VersionInterval,
};

use crate::hos::ncm::NcmContentInfo;

#[derive(Copy, Clone, PartialEq, Eq, Debug, Default)]
#[repr(C)]
pub struct ContentId {
    pub buffer: [u8; 0x10],
}

impl PartialOrd for ContentId {
    fn partial_cmp(&self, other: &Self) -> Option<core::cmp::Ordering> {
        u128::from_le_bytes(self.buffer).partial_cmp(&u128::from_le_bytes(other.buffer))
    }
}

impl Ord for ContentId {
    fn cmp(&self, other: &Self) -> core::cmp::Ordering {
        u128::from_le_bytes(self.buffer).cmp(&u128::from_le_bytes(other.buffer))
    }
}

impl ContentId {
    pub fn new(buffer: [u8; 0x10]) -> Self {
        Self { buffer }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug, Default)]
#[repr(C)]
pub struct PlaceholderId {
    pub buffer: [u8; 0x10],
}

impl PlaceholderId {
    pub fn new(buffer: [u8; 0x10]) -> Self {
        Self { buffer }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Default)]
#[repr(C)]
pub struct ProgramId(pub u64);

impl Display for ProgramId {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_fmt(format_args!("{:016x}", self.0))
    }
}

impl Debug for ProgramId {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_fmt(format_args!("{:016x}", self.0))
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Debug, Default)]
#[repr(u8)]
pub enum ContentMetaType {
    #[default]
    Unknown,
    SystemProgram,
    SystemData,
    SystemUpdate,
    BootImagePackage,
    BootImagePackageSafe,
    Application = 0x80,
    Patch,
    AddOnContent,
    Delta,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug, Default)]
#[repr(u8)]
pub enum ContentInstallType {
    Full,
    FragmentOnly,
    #[default]
    Unknown = 0x7,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug, Default)]
#[repr(C)]
pub struct ContentMetaKey {
    pub program_id: ProgramId,
    pub version: u32,
    pub content_meta_type: ContentMetaType,
    pub content_install_type: ContentInstallType,
    pub _padding: u16,
}

#[derive(Copy, Clone, PartialEq, Eq, Debug, Default)]
#[repr(u8)]
pub enum StorageId {
    None,
    Host,
    GameCard,
    BuiltInSystem,
    BuiltInUser,
    SdCard,
    #[default]
    Any,
}

impl Display for StorageId {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_str(match self {
            StorageId::None => "None",
            StorageId::Host => "Host",
            StorageId::GameCard => "GameCard",
            StorageId::BuiltInSystem => "System",
            StorageId::BuiltInUser => "NAND",
            StorageId::SdCard => "SD Card",
            StorageId::Any => "Any",
        })
    }
}

ipc_sf_define_interface_trait! {
    trait IContentMetaDatabase {
        content_meta_database_set [0, VersionInterval::all()]: (content_meta_key: ContentMetaKey, buffer: InMapAliasBuffer<u8>, size: u64) => ();
        content_meta_database_list_content_info [4, VersionInterval::all()]: (out_buffer: OutMapAliasBuffer<NcmContentInfo>, content_meta_key: ContentMetaKey, index: i32) => (entries_read: i32);
        content_meta_database_list[5, VersionInterval::all()]: (out_buffer: OutMapAliasBuffer<ContentMetaKey>, meta_type: EnumAsPrimitiveType<ContentMetaType, u32>, program_id: ProgramId, program_id_low: ProgramId, program_id_high: ProgramId) => (total_entry_count: i32, read_entry_count: i32);
        content_meta_database_get_size [10, VersionInterval::all()]: (content_meta_key: ContentMetaKey) => (size: u64);
        content_meta_database_commit [15, VersionInterval::all()]: () => ();
    }
}

ipc_client_define_object_default!(ContentMetaDatabase);

impl IContentMetaDatabase for ContentMetaDatabase {
    #[allow(unused_parens)]
    fn content_meta_database_get_size(
        &mut self,
        content_meta_key: ContentMetaKey,
    ) -> Result<(u64)> {
        ipc_client_send_request_command!([self.session.object_info; 10] (content_meta_key) => (size: u64))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn content_meta_database_set(
        &mut self,
        content_meta_key: ContentMetaKey,
        buffer: InMapAliasBuffer<u8>,
        size: u64,
    ) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 0] (content_meta_key, buffer, size) => ())
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn content_meta_database_commit(&mut self) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 15] () => ())
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn content_meta_database_list(
        &mut self,
        out_buffer: OutMapAliasBuffer<ContentMetaKey>,
        meta_type: EnumAsPrimitiveType<ContentMetaType, u32>,
        program_id: ProgramId,
        program_id_low: ProgramId,
        program_id_high: ProgramId,
    ) -> Result<(i32, i32)> {
        ipc_client_send_request_command!([self.session.object_info; 5] (out_buffer, meta_type, program_id, program_id_low, program_id_high) => (total_entry_count: i32, read_entry_count: i32))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn content_meta_database_list_content_info(
        &mut self,
        buffer: OutMapAliasBuffer<NcmContentInfo>,
        content_meta_key: ContentMetaKey,
        index: i32,
    ) -> Result<(i32)> {
        ipc_client_send_request_command!([self.session.object_info; 4] (buffer, index, content_meta_key) => (entries_read: i32))
    }
}

ipc_sf_define_interface_trait! {
    trait IContentStorage {
        create_placeholder [1, VersionInterval::all()]: (content_id: ContentId, placeholder_id: PlaceholderId, file_size: i64) => ();
        delete_placeholder [2, VersionInterval::all()]: (placeholder_id: PlaceholderId) => ();
        write_placeholder [4, VersionInterval::all()]: (placeholder_id: PlaceholderId, offset: u64, buffer: InMapAliasBuffer<u8>) => ();
        register [5, VersionInterval::all()]: (content_id: ContentId, placeholder_id: PlaceholderId) => ();
        delete [6, VersionInterval::all()]: (content_id: ContentId) => ();
        has [7, VersionInterval::all()]: (content_id: ContentId) => (res: bool);
        get_path [8, VersionInterval::all()]: (content_id: ContentId, out_path: OutFixedPointerBuffer<Path>) => ();
        get_size_from_content_id [14, VersionInterval::all()]: (content_id: ContentId) => (size: u64);
        read_content_id_file [18, VersionInterval::all()]: (out_buffer: OutMapAliasBuffer<u8>, content_id: ContentId, file_offset: i64) => ();
    }
}

ipc_client_define_object_default!(ContentStorage);

impl ContentStorage {
    pub fn get_path_string(&mut self, content_id: ContentId) -> Result<String> {
        let mut path = Path::new();
        self.get_path(content_id, Buffer::from_mut_var(&mut path))?;
        path.get_string()
    }
}

impl IContentStorage for ContentStorage {
    #[allow(unused_parens)]
    fn create_placeholder(
        &mut self,
        content_id: ContentId,
        place_holder_id: PlaceholderId,
        file_size: i64,
    ) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 1] (content_id, place_holder_id, file_size) => ())
    }

    #[allow(unused_parens)]
    fn delete_placeholder(&mut self, place_holder_id: PlaceholderId) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 2] (place_holder_id) => ())
    }

    #[allow(unused_parens)]
    fn write_placeholder(
        &mut self,
        place_holder_id: PlaceholderId,
        offset: u64,
        buffer: InMapAliasBuffer<u8>,
    ) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 4] (place_holder_id, offset, buffer) => ())
    }

    #[allow(unused_parens)]
    fn delete(&mut self, content_id: ContentId) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 6] (content_id) => ())
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn register(&mut self, content_id: ContentId, place_holder_id: PlaceholderId) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 5] (content_id, place_holder_id) => ())
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn get_path(
        &mut self,
        content_id: ContentId,
        out_path: OutFixedPointerBuffer<Path>,
    ) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 8] (content_id, out_path) => ())
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn has(&mut self, content_id: ContentId) -> Result<(bool)> {
        ipc_client_send_request_command!([self.session.object_info; 7] (content_id) => (res: bool))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn read_content_id_file(
        &mut self,
        out_buffer: OutMapAliasBuffer<u8>,
        content_id: ContentId,
        file_offset: i64,
    ) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 18] (out_buffer, content_id, file_offset) => ())
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn get_size_from_content_id(&mut self, content_id: ContentId) -> Result<(u64)> {
        ipc_client_send_request_command!([self.session.object_info; 14] (content_id) => (size: u64))
    }
}

ipc_sf_define_interface_trait! {
    trait IContentManager {
        open_content_storage [4, VersionInterval::all()]: (storage_id: StorageId) => (content_storage: Shared<ContentStorage>);
        open_content_meta_database [5, VersionInterval::all()]: (storage_id: StorageId) => (content_meta_database: Shared<ContentMetaDatabase>);
    }
}

ipc_client_define_object_default!(ContentManager);

impl IContentManager for ContentManager {
    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn open_content_storage(&mut self, storage_id: StorageId) -> Result<(Shared<ContentStorage>)> {
        ipc_client_send_request_command!([self.session.object_info; 4] (storage_id) => (content_storage: Shared<ContentStorage>))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn open_content_meta_database(
        &mut self,
        storage_id: StorageId,
    ) -> Result<(Shared<ContentMetaDatabase>)> {
        ipc_client_send_request_command!([self.session.object_info; 5] (storage_id) => (content_meta_database: Shared<ContentMetaDatabase>))
    }
}

impl IService for ContentManager {
    fn get_name() -> ServiceName {
        ServiceName::new("ncm")
    }

    fn as_domain() -> bool {
        false
    }

    fn post_initialize(&mut self) -> Result<()> {
        Ok(())
    }
}

static mut G_NCM_SESSION: nx::sync::Locked<Option<nx::mem::Shared<dyn IContentManager>>> =
    nx::sync::Locked::new(false, None);

pub fn initialise_ncm_session(content_manager: Shared<ContentManager>) {
    unsafe {
        G_NCM_SESSION.set(Some(content_manager));
    }
}

pub fn finalise_ncm_session() {
    unsafe {
        G_NCM_SESSION.set(None);
    }
}

#[inline]
pub fn get_ncm_session() -> Result<&'static nx::mem::Shared<dyn IContentManager>> {
    unsafe {
        G_NCM_SESSION
            .get()
            .as_ref()
            .ok_or(ResultNotInitialized::make())
    }
}
