use alloc::vec;
use nx::{
    ipc::sf::{self, EnumAsPrimitiveType, InMapAliasBuffer, OutMapAliasBuffer},
    ipc_client_define_object_default, ipc_client_send_request_command,
    ipc_sf_define_interface_trait,
    mem::Shared,
    rc::ResultNotInitialized,
    result::{Result, ResultBase},
    service::{sm::ServiceName, IService},
    version::VersionInterval,
};

use crate::install::formats::nacp::Nacp;

use super::ncm::{ContentMetaKey, ContentMetaType, ProgramId, StorageId};

#[derive(Copy, Clone, PartialEq, Eq, Debug)]
#[repr(u8)]
pub enum NsApplicationRecordType {
    Installed = 0x3,
    GamecardMissing = 0x5,
    Archived = 0xB,
}

#[derive(Copy, Clone)]
#[repr(C)]
pub struct ContentStorageRecord {
    pub meta_record: ContentMetaKey,
    pub storage_id: EnumAsPrimitiveType<StorageId, u64>,
}

impl Default for ContentStorageRecord {
    fn default() -> Self {
        Self {
            meta_record: Default::default(),
            storage_id: EnumAsPrimitiveType::from(StorageId::Any),
        }
    }
}

#[derive(Copy, Clone, Default)]
#[repr(C)]
pub struct ApplicationRecord {
    pub application_id: ProgramId,
    pub status: u8,
    _reserved: [u8; 0xF],
}

#[derive(Copy, Clone)]
#[repr(u8)]
pub enum ApplicationControlSource {
    CacheOnly,
    Storage,
    StorageOnly,
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct ApplicationControlData {
    pub nacp: Nacp,
    pub icon: [u8; 0x20000],
}

impl Default for ApplicationControlData {
    fn default() -> Self {
        Self {
            nacp: Default::default(),
            icon: [0u8; 0x20000],
        }
    }
}

#[derive(Debug, Clone, Copy, Default)]
#[repr(C)]
pub struct ApplicationContentMetaStatus {
    pub content_meta_type: ContentMetaType,
    pub storage_id: StorageId,
    _res_2: u16,
    pub version: u32,
    pub program_id: ProgramId,
}

ipc_sf_define_interface_trait! {
    trait IApplicationManager {
        list_application_records [0, VersionInterval::all()]: (out_buffer: OutMapAliasBuffer<ApplicationRecord>, offset: u64) => (count: u32);
        generate_application_record_count [1, VersionInterval::all()]: () => (count: u32);
        push_application_record [16, VersionInterval::all()]: (record_type: u8, application_id: ProgramId, in_buffer: InMapAliasBuffer<ContentStorageRecord>) => ();
        list_application_content_storage_records [17, VersionInterval::all()]: (out_buffer: OutMapAliasBuffer<ContentStorageRecord>, index: i32, application_id: ProgramId) => (out_entry_count: i32);
        delete_application_record [27, VersionInterval::all()]: (application_id: ProgramId) => ();
        get_application_control_data [400, VersionInterval::all()]: (control_source: EnumAsPrimitiveType<ApplicationControlSource, u64>, application_id: ProgramId, out_buffer: OutMapAliasBuffer<ApplicationControlData>) => (size: u32);
        count_application_content_storage_records [600, VersionInterval::all()]: (application_id: ProgramId) => (out_entry_count: i32);
        list_application_content_meta_status [601, VersionInterval::all()]: (out_buffer: OutMapAliasBuffer<ApplicationContentMetaStatus>, index: i32, application_id: ProgramId) => (out_entry_count: i32);
    }
}

ipc_client_define_object_default!(ApplicationManager);

impl IApplicationManager for ApplicationManager {
    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn push_application_record(
        &mut self,
        record_type: u8,
        application_id: ProgramId,
        in_buffer: InMapAliasBuffer<ContentStorageRecord>,
    ) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 16] (record_type, application_id, in_buffer) => ())
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn list_application_records(
        &mut self,
        out_buffer: OutMapAliasBuffer<ApplicationRecord>,
        offset: u64,
    ) -> Result<(u32)> {
        ipc_client_send_request_command!([self.session.object_info; 0] (out_buffer, offset) => (count: u32))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn generate_application_record_count(&mut self) -> Result<(u32)> {
        ipc_client_send_request_command!([self.session.object_info; 1] () => (count: u32))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn get_application_control_data(
        &mut self,
        control_source: EnumAsPrimitiveType<ApplicationControlSource, u64>,
        application_id: ProgramId,
        out_buffer: OutMapAliasBuffer<ApplicationControlData>,
    ) -> Result<(u32)> {
        ipc_client_send_request_command!([self.session.object_info; 400] (control_source, application_id, out_buffer) => (count: u32))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn list_application_content_meta_status(
        &mut self,
        out_buffer: OutMapAliasBuffer<ApplicationContentMetaStatus>,
        index: i32,
        application_id: ProgramId,
    ) -> Result<(i32)> {
        ipc_client_send_request_command!([self.session.object_info; 601] (out_buffer, index, application_id) => (out_entry_count: i32))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn list_application_content_storage_records(
        &mut self,
        out_buffer: OutMapAliasBuffer<ContentStorageRecord>,
        index: i32,
        application_id: ProgramId,
    ) -> Result<(i32)> {
        ipc_client_send_request_command!([self.session.object_info; 17] (out_buffer, index, application_id) => (out_entry_count: i32))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn count_application_content_storage_records(
        &mut self,
        application_id: ProgramId,
    ) -> Result<(i32)> {
        ipc_client_send_request_command!([self.session.object_info; 600] (application_id) => (out_entry_count: i32))
    }

    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn delete_application_record(&mut self, application_id: ProgramId) -> Result<()> {
        ipc_client_send_request_command!([self.session.object_info; 27] (application_id) => ())
    }
}

ipc_sf_define_interface_trait! {
    trait IServiceGetterInterface {
        get_application_manager_interface [7996, VersionInterval::from(nx::version::Version::new(3, 0, 0))]: () => (out: Shared<ApplicationManager>);
    }
}

ipc_client_define_object_default!(ServiceGetterInterface);

impl IServiceGetterInterface for ServiceGetterInterface {
    #[allow(unused_parens)]
    #[allow(clippy::too_many_arguments)]
    fn get_application_manager_interface(&mut self) -> Result<(Shared<ApplicationManager>)> {
        ipc_client_send_request_command!([self.session.object_info; 7996] () => (out: Shared<ApplicationManager>))
    }
}

impl IService for ServiceGetterInterface {
    fn get_name() -> ServiceName {
        ServiceName::new("ns:am2")
    }

    fn as_domain() -> bool {
        false
    }

    fn post_initialize(&mut self) -> Result<()> {
        Ok(())
    }
}

static mut G_NS_SESSION: nx::sync::Locked<Option<Shared<dyn IServiceGetterInterface>>> =
    nx::sync::Locked::new(false, None);

static mut G_NS_AM_SESSION: nx::sync::Locked<Option<Shared<dyn IApplicationManager>>> =
    nx::sync::Locked::new(false, None);

pub fn initialise_ns_session(sgi: Shared<dyn IServiceGetterInterface>) -> Result<()> {
    let am_session = sgi.get().get_application_manager_interface()?;

    unsafe {
        G_NS_SESSION.set(Some(sgi));
        G_NS_AM_SESSION.set(Some(am_session));
    }

    Ok(())
}

pub fn finalise_ns_session() {
    unsafe {
        G_NS_AM_SESSION.set(None);
        G_NS_SESSION.set(None);
    }
}

#[inline]
pub fn get_ns_am_session() -> Result<&'static Shared<dyn IApplicationManager>> {
    unsafe {
        G_NS_AM_SESSION
            .get()
            .as_ref()
            .ok_or(ResultNotInitialized::make())
    }
}
