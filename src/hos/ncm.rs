use core::fmt::Display;

use super::hipc::ncm::ContentId;

#[derive(Debug, Clone, Copy, Default)]
#[repr(C)]
pub struct NcmContentInfo {
    pub content_id: ContentId,
    pub size_low: u32,
    pub size_high: u8,
    pub attr: u8,
    pub content_type: NcmContentType,
    pub id_offset: u8,
}

#[derive(Debug, Clone, Copy, Default, PartialEq, Eq)]
#[repr(u8)]
pub enum NcmContentType {
    #[default]
    Meta,
    Program,
    Data,
    Control,
    HtmlDocument,
    LegalInformation,
    DeltaFragment,
}

impl Display for NcmContentType {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        f.write_str(match self {
            NcmContentType::Meta => "Meta",
            NcmContentType::Program => "Program",
            NcmContentType::Data => "Data",
            NcmContentType::Control => "Control",
            NcmContentType::HtmlDocument => "HtmlDocument",
            NcmContentType::LegalInformation => "LegalInformation",
            NcmContentType::DeltaFragment => "DeltaFragment",
        })
    }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)]
pub struct NcmContentMetaHeader {
    pub extended_header_size: u16,
    pub content_count: u16,
    pub content_meta_count: u16,
    pub attributes: u8,
    pub storage_id: u8,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)]
pub struct NcmPatchMetaExtendedHeader {
    pub application_id: u64,
    pub required_system_version: u32,
    pub extended_data_size: u32,
    pub reserved: u64,
}
