use alloc::{format, string::String, vec, vec::Vec};
use nx::{
    fs::{FileOpenMode, FileReadOption, FileSystem},
    ipc::sf::{Buffer, EnumAsPrimitiveType},
    result::Result,
};

use crate::{
    hos::{
        fs::nca::NcaFileSystem,
        hipc::{
            ncm::{
                get_ncm_session, ContentId, ContentMetaKey, IContentMetaDatabase, ProgramId,
                StorageId,
            },
            ns::{get_ns_am_session, ContentStorageRecord},
        },
        ncm::{NcmContentInfo, NcmContentType},
        title_id_to_base,
    },
    ui::logger_task::LoggerTx,
};

use self::formats::cnmt::get_install_content_metadata;

pub mod formats;
pub mod nca;
pub mod nsp;
pub mod sd;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct InstallSuccess {
    pub nca_count: usize,
    pub ticket_count: usize,
    pub certificate_count: usize,
}

impl InstallSuccess {
    pub fn new(nca_count: usize, ticket_count: usize, certificate_count: usize) -> Self {
        Self {
            nca_count,
            ticket_count,
            certificate_count,
        }
    }
}
pub trait Installer {
    fn install(&self, tx: &LoggerTx, storage_id: StorageId) -> Result<Option<InstallSuccess>>;

    fn install_nca(
        &self,
        tx: &LoggerTx,
        nca_path: String,
        storage_id: StorageId,
    ) -> Result<Option<[u8; 0x10]>>;
    fn fetch_ticket_cert(&self, tx: &LoggerTx) -> Result<Option<(vec::Vec<u8>, vec::Vec<u8>)>>;

    fn install_ticket_cert(&self, tx: &LoggerTx) -> Result<Option<()>> {
        let (ticket_content, certificate_content) = match self.fetch_ticket_cert(tx)? {
            Some(tuple) => tuple,
            None => {
                return Ok(None);
            }
        };
        crate::hos::hipc::es::get_es_session()?
            .get()
            .import_ticket(
                Buffer::from_ptr(ticket_content.as_ptr(), ticket_content.len()),
                Buffer::from_ptr(certificate_content.as_ptr(), certificate_content.len()),
            )
            .map(Some)
    }
}

fn install_registered_cnmt(
    tx: &LoggerTx,
    cnmt_nca_id: [u8; 0x10],
    storage_id: StorageId,
) -> Result<Option<Vec<NcmContentInfo>>> {
    let content_storage = get_ncm_session()?.get().open_content_storage(storage_id)?;
    let cnmt_content_path = content_storage
        .get()
        .get_path_string(ContentId::new(cnmt_nca_id))?;

    let cnmt_buffer = match read_cnmt_from_nca(tx, cnmt_content_path)? {
        Some(buffer) => buffer,
        None => return Ok(None),
    };

    let cnmt_size = cnmt_buffer.len();

    let cnmt_content_info = NcmContentInfo {
        content_id: ContentId::new(cnmt_nca_id),
        size_low: cnmt_size as u32,
        size_high: (cnmt_size >> (4 * 8)) as u8,
        attr: 0,
        content_type: NcmContentType::Meta,
        id_offset: 0,
    };

    let (content_meta_header, content_meta_buffer, content_info_list) =
        match get_install_content_metadata(&cnmt_buffer, &cnmt_content_info) {
            Some(tuple) => tuple,
            None => {
                tx.tx("Buffer too small, get_install_content_metadata failed");
                return Ok(None);
            }
        };

    let content_meta_header = &content_meta_header;
    let content_meta_buffer: &[u8] = &content_meta_buffer;
    let base_title_id = title_id_to_base(content_meta_header.title_id, content_meta_header.r#type);
    let content_meta_key = ContentMetaKey {
        program_id: ProgramId(content_meta_header.title_id),
        version: content_meta_header.version,
        content_meta_type: content_meta_header.r#type,
        content_install_type: crate::hos::hipc::ncm::ContentInstallType::Full,
        _padding: 0,
    };

    let content_meta_database = get_ncm_session()?
        .get()
        .open_content_meta_database(storage_id)?;
    content_meta_database.get().content_meta_database_set(
        content_meta_key,
        Buffer::from_ptr(content_meta_buffer.as_ptr(), content_meta_buffer.len()),
        content_meta_buffer.len() as u64,
    )?;
    content_meta_database.get().content_meta_database_commit()?;

    let application_content_storage_record_count = get_ns_am_session()?
        .get()
        .count_application_content_storage_records(ProgramId(base_title_id))
        .unwrap_or_default();

    let mut content_storage_records = vec![];

    if application_content_storage_record_count > 0 {
        let mut content_storage_record_list = vec![
            ContentStorageRecord::default();
            application_content_storage_record_count as usize
        ];
        let read_count = get_ns_am_session()?
            .get()
            .list_application_content_storage_records(
                Buffer::from_mut_array(&mut content_storage_record_list),
                0,
                ProgramId(base_title_id),
            )?;

        for key in content_storage_record_list
            .into_iter()
            .take(read_count.max(0) as usize)
        {
            content_storage_records.push(key)
        }
    }

    content_storage_records.push(ContentStorageRecord {
        meta_record: content_meta_key,
        storage_id: EnumAsPrimitiveType::from(storage_id),
    });

    let _ = get_ns_am_session()?
        .get()
        .delete_application_record(ProgramId(base_title_id));

    get_ns_am_session()?.get().push_application_record(
        1,
        ProgramId(base_title_id),
        Buffer::from_array(&content_storage_records),
    )?;

    Ok(Some(content_info_list))
}

fn read_cnmt_from_nca(tx: &LoggerTx, content_path: String) -> Result<Option<Vec<u8>>> {
    let mut nca_fs =
        NcaFileSystem::new(content_path, nx::service::fsp::FileSystemProxyType::Meta, 0)?;

    let cnmt_file_name = match nca_fs.get_file_by_ext(".cnmt")? {
        Some(name) => name,
        None => {
            tx.tx("CNMT NCA lacks a CNMT");
            return Ok(None);
        }
    };
    tx.tx(format!("Found CNMT: {}", cnmt_file_name));

    let cnmt_file = nca_fs.open_file(format!("/{}", cnmt_file_name), FileOpenMode::Read())?;
    let cnmt_size = cnmt_file.get().get_size()?;

    if cnmt_size > 4_000_000 {
        tx.tx(format!(
            "Error: CNMT {} bytes too large",
            cnmt_size - 4_000_000
        ));
        return Ok(None);
    }

    let mut cnmt_buffer = vec![0u8; cnmt_size];
    cnmt_file.get().read(
        0,
        cnmt_buffer.as_mut_ptr(),
        cnmt_size,
        FileReadOption::None(),
    )?;

    Ok(Some(cnmt_buffer))
}
