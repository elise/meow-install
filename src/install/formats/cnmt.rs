use alloc::{vec, vec::Vec};

use crate::hos::{
    hipc::ncm::ContentMetaType,
    ncm::{NcmContentInfo, NcmContentMetaHeader, NcmContentType},
};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(C)]
pub struct PackagedContentMetaHeader {
    pub title_id: u64,
    pub version: u32,
    pub r#type: ContentMetaType,
    pub _res_0d: u8,
    pub header: NcmContentMetaHeader,
    pub _res_16: [u8; 2],
    pub required_download_system_version: u32,
    pub _res_1c: [u8; 4],
}

impl PackagedContentMetaHeader {
    pub fn new(buffer: [u8; 0x20]) -> Self {
        unsafe { core::mem::transmute(buffer) }
    }

    pub fn from_slice(slice: &[u8]) -> Option<Self> {
        if slice.len() < 0x20 {
            return None;
        }

        let mut buffer: [u8; 0x20] = [0; 0x20];

        buffer.copy_from_slice(&slice[..0x20]);

        Some(Self::new(buffer))
    }
}

#[derive(Debug, Clone, Copy, Default)]
#[repr(C)]
pub struct PackagedContentInfo {
    pub hash: [u8; 0x20],
    pub content_info: NcmContentInfo,
}

impl PackagedContentInfo {
    pub fn new(buffer: [u8; 0x38]) -> Self {
        unsafe { core::mem::transmute(buffer) }
    }

    pub fn from_slice(slice: &[u8]) -> Option<Self> {
        if slice.len() < 0x38 {
            return None;
        }

        let mut buffer: [u8; 0x38] = [0; 0x38];

        buffer.copy_from_slice(&slice[..0x38]);

        Some(Self::new(buffer))
    }
}

pub fn get_header_and_content_info_list(
    cnmt_buffer: &[u8],
) -> Option<(PackagedContentMetaHeader, Vec<NcmContentInfo>)> {
    let packaged_content_meta_header = PackagedContentMetaHeader::from_slice(cnmt_buffer)?;

    let mut out = Vec::new();

    let mut offset = core::mem::size_of::<PackagedContentMetaHeader>()
        + packaged_content_meta_header.header.extended_header_size as usize;

    for _ in 0..packaged_content_meta_header.header.content_count {
        let slice = &cnmt_buffer[offset..offset + core::mem::size_of::<PackagedContentInfo>()];
        let packaged_content_info = PackagedContentInfo::from_slice(slice)?;

        if packaged_content_info.content_info.content_type != NcmContentType::DeltaFragment {
            out.push(packaged_content_info.content_info);
        }

        offset += core::mem::size_of::<PackagedContentInfo>();
    }

    Some((packaged_content_meta_header, out))
}

pub fn get_install_content_metadata(
    cnmt_buffer: &[u8],
    content_info: &NcmContentInfo,
) -> Option<(PackagedContentMetaHeader, Vec<u8>, Vec<NcmContentInfo>)> {
    let (packaged_content_meta_header, content_info_list) =
        get_header_and_content_info_list(cnmt_buffer)?;

    let out_size = core::mem::size_of::<NcmContentMetaHeader>()
        + packaged_content_meta_header.header.extended_header_size as usize
        + ((content_info_list.len() + 1) * core::mem::size_of::<NcmContentInfo>())
        + match packaged_content_meta_header.r#type {
            ContentMetaType::SystemUpdate => {
                let extended_data_size_offset = core::mem::size_of::<PackagedContentMetaHeader>();
                u32::from_le_bytes([
                    cnmt_buffer[extended_data_size_offset],
                    cnmt_buffer[extended_data_size_offset + 1],
                    cnmt_buffer[extended_data_size_offset + 2],
                    cnmt_buffer[extended_data_size_offset + 3],
                ]) as usize
            }
            ContentMetaType::Patch => {
                let extended_data_size_offset =
                    core::mem::size_of::<PackagedContentMetaHeader>() + 0xC;
                u32::from_le_bytes([
                    cnmt_buffer[extended_data_size_offset],
                    cnmt_buffer[extended_data_size_offset + 1],
                    cnmt_buffer[extended_data_size_offset + 2],
                    cnmt_buffer[extended_data_size_offset + 3],
                ]) as usize
            }
            ContentMetaType::Delta => {
                let extended_data_size_offset =
                    core::mem::size_of::<PackagedContentMetaHeader>() + 0x8;
                u32::from_le_bytes([
                    cnmt_buffer[extended_data_size_offset],
                    cnmt_buffer[extended_data_size_offset + 1],
                    cnmt_buffer[extended_data_size_offset + 2],
                    cnmt_buffer[extended_data_size_offset + 3],
                ]) as usize
            }
            _ => 0,
        };

    let mut out = vec![0u8; out_size];

    let ncm_content_meta_header = NcmContentMetaHeader {
        extended_header_size: packaged_content_meta_header.header.extended_header_size,
        content_count: content_info_list.len() as u16 + 1,
        content_meta_count: packaged_content_meta_header.header.content_meta_count,
        attributes: packaged_content_meta_header.header.attributes,
        storage_id: packaged_content_meta_header.header.storage_id,
    };

    let mut offset = 0;
    out[offset..offset + core::mem::size_of::<NcmContentMetaHeader>()].copy_from_slice(&unsafe {
        core::mem::transmute::<_, [u8; core::mem::size_of::<NcmContentMetaHeader>()]>(
            ncm_content_meta_header,
        )
    });
    offset += core::mem::size_of::<NcmContentMetaHeader>();

    out[offset..offset + packaged_content_meta_header.header.extended_header_size as usize]
        .copy_from_slice(
            &cnmt_buffer[offset
                ..offset + packaged_content_meta_header.header.extended_header_size as usize],
        );
    offset += packaged_content_meta_header.header.extended_header_size as usize;

    out[offset..offset + core::mem::size_of::<NcmContentInfo>()].copy_from_slice(&unsafe {
        core::mem::transmute_copy::<_, [u8; core::mem::size_of::<NcmContentInfo>()]>(content_info)
    });
    offset += core::mem::size_of::<NcmContentInfo>();

    for content_info in &content_info_list {
        out[offset..offset + core::mem::size_of::<NcmContentInfo>()].copy_from_slice(&unsafe {
            core::mem::transmute_copy::<_, [u8; core::mem::size_of::<NcmContentInfo>()]>(
                content_info,
            )
        });
        offset += core::mem::size_of::<NcmContentInfo>();
    }

    Some((packaged_content_meta_header, out, content_info_list))
}
