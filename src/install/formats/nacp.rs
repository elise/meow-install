#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct Nacp {
    pub titles: [NacpTitle; 16],
    _res: [u8; 0x1000],
}

impl Default for Nacp {
    fn default() -> Self {
        Self {
            titles: Default::default(),
            _res: [0u8; 0x1000],
        }
    }
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
pub struct NacpTitle {
    pub application_name: [u8; 0x200],
    pub application_publisher: [u8; 0x100],
}

impl Default for NacpTitle {
    fn default() -> Self {
        Self {
            application_name: [0u8; 0x200],
            application_publisher: [0u8; 0x100],
        }
    }
}
