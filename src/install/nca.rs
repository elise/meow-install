use alloc::{
    collections::BTreeMap,
    format,
    string::{String, ToString},
    vec::Vec,
};
use nx::{fs::DirectoryOpenMode, mem::Shared, result::Result};

use crate::{
    hos::{
        hipc::ncm::{
            get_ncm_session, ContentId, ContentStorage, IContentStorage, PlaceholderId, StorageId,
        },
        parse_nca_id,
    },
    ui::logger_task::LoggerTx,
};

use super::{install_registered_cnmt, InstallSuccess, Installer};

pub struct SdNcaInstaller {
    path: String,
    cnmt_path: String,
    ticket_path: Option<String>,
    cert_path: Option<String>,

    entries: BTreeMap<[u8; 0x10], String>,
}

impl SdNcaInstaller {
    pub fn new(tx: &LoggerTx, path: String) -> Result<Option<Self>> {
        let mut dir = nx::fs::open_directory(path.clone(), DirectoryOpenMode::ReadFiles())?;

        let mut cnmt_path = None;
        let mut ticket_path = None;
        let mut cert_path = None;

        let mut entries = alloc::collections::BTreeMap::new();

        while let Ok(Some(dd)) = dir.read_next() {
            let name = dd.name.get_str()?;

            if name.starts_with('.') {
                continue;
            }

            if name.ends_with(".nca") {
                let nca_id = name
                    .split('/')
                    .last()
                    .unwrap_or(name)
                    .trim_end_matches(".nca")
                    .trim_end_matches(".cnmt");
                let nca_id = parse_nca_id(tx, nca_id);

                if let Some(id) = nca_id {
                    entries.insert(id, name.to_string());
                }
            }

            if name.ends_with(".cnmt.nca") {
                cnmt_path = Some(format!("{}/{}", path, name));
            } else if name.ends_with(".tik") {
                ticket_path = Some(format!("{}/{}", path, name));
            } else if name.ends_with(".cert") {
                cert_path = Some(format!("{}/{}", path, name));
            }
        }

        Ok(cnmt_path.map(|cnmt_path| Self {
            path,
            cnmt_path,
            ticket_path,
            cert_path,
            entries,
        }))
    }
}

impl Installer for SdNcaInstaller {
    fn install(&self, tx: &LoggerTx, storage_id: StorageId) -> Result<Option<InstallSuccess>> {
        let cnmt_nca_id = match self.install_nca(tx, self.cnmt_path.clone(), storage_id)? {
            Some(id) => id,
            None => {
                tx.tx("Error: Failed to install CNMT");
                return Ok(None);
            }
        };

        let content_info_list: Vec<crate::hos::ncm::NcmContentInfo> =
            match install_registered_cnmt(tx, cnmt_nca_id, storage_id)? {
                Some(content_info_list) => content_info_list,
                None => {
                    tx.tx("Failed to read CNMT");
                    return Ok(None);
                }
            };

        let mut ticket_cert_count = 1usize;
        if let Err(why) = self.install_ticket_cert(tx) {
            tx.tx(format!(
                "Failed to install ticket/cert ({}), continuing anyways",
                why
            ));
            ticket_cert_count = 0;
        }

        let mut installed = alloc::vec![cnmt_nca_id];
        for content_info in content_info_list {
            let nca_name = match self.entries.get(&content_info.content_id.buffer) {
                Some(path) => path.clone(),
                None => {
                    tx.tx(format!(
                        "NSP missing NCA {}\nUninstalling and exiting...",
                        hex::encode(content_info.content_id.buffer)
                    ));

                    let content_storage =
                        get_ncm_session()?.get().open_content_storage(storage_id)?;

                    for installed in installed {
                        content_storage.get().delete(ContentId::new(installed))?;
                    }

                    return Ok(None);
                }
            };

            match self.install_nca(tx, format!("{}/{}", self.path, nca_name), storage_id)? {
                Some(nca_id) => {
                    installed.push(nca_id);
                }
                None => {
                    tx.tx(format!(
                        "Installation of NCA {} failed\nUninstalling and exiting...",
                        hex::encode(content_info.content_id.buffer)
                    ));

                    let content_storage =
                        get_ncm_session()?.get().open_content_storage(storage_id)?;

                    for installed in installed {
                        content_storage.get().delete(ContentId::new(installed))?;
                    }

                    return Ok(None);
                }
            };
        }

        Ok(Some(InstallSuccess::new(
            installed.len(),
            ticket_cert_count,
            ticket_cert_count,
        )))
    }

    fn install_nca(
        &self,
        tx: &LoggerTx,
        nca_path: String,
        storage_id: StorageId,
    ) -> Result<Option<[u8; 0x10]>> {
        let nca_id_str = nca_path
            .split('/')
            .last()
            .unwrap_or(nca_path.as_str())
            .trim_end_matches(".nca")
            .trim_end_matches(".cnmt");
        let nca_id = match parse_nca_id(tx, nca_id_str) {
            Some(nca_id) => nca_id,
            None => {
                tx.tx("Error install NCA: Couldn't parse NCA ID");
                return Ok(None);
            }
        };

        let content_storage = get_ncm_session()?.get().open_content_storage(storage_id)?;

        if let Ok(true) = content_storage.get().has(ContentId::new(nca_id)) {
            tx.tx(format!("NCA {} already registered!", nca_id_str));
            return Ok(Some(nca_id));
        }

        let _ = content_storage
            .get()
            .delete_placeholder(PlaceholderId::new(nca_id));

        let mut nca_file = nx::fs::open_file(nca_path.clone(), nx::fs::FileOpenOption::Read())?;
        let nca_size = nca_file.get_size()? as i64;

        content_storage.get().create_placeholder(
            ContentId::new(nca_id),
            PlaceholderId::new(nca_id),
            nca_size,
        )?;

        copy_nca_to_placeholder(tx, nca_path, content_storage.clone(), nca_id)?;

        if let Err(why) = content_storage
            .get()
            .register(ContentId::new(nca_id), PlaceholderId::new(nca_id))
        {
            tx.tx(format!(
                "Failed to register placeholder ({}), deleting...",
                why
            ));
            content_storage
                .get()
                .delete_placeholder(PlaceholderId::new(nca_id))?;
            tx.tx("Deleted placeholder");
            return Ok(None);
        }

        Ok(Some(nca_id))
    }

    fn fetch_ticket_cert(
        &self,
        tx: &LoggerTx,
    ) -> Result<Option<(alloc::vec::Vec<u8>, alloc::vec::Vec<u8>)>> {
        let (ticket_path, cert_path) = match (self.ticket_path.clone(), self.cert_path.clone()) {
            (Some(left_side), Some(right_side)) => (left_side, right_side),
            _ => {
                return Ok(None);
            }
        };

        let ticket_content = {
            let mut file = nx::fs::open_file(ticket_path, nx::fs::FileOpenOption::Read())?;
            let size = file.get_size()?;

            if size > 1_000_000 {
                tx.tx(format!(
                    "Error: Ticket {} bytes too large",
                    size - 1_000_000
                ));
                return Ok(None);
            }

            let mut buffer = alloc::vec![0u8; size];
            file.read_array(&mut buffer)?;
            buffer
        };

        let certificate_content = {
            let mut file = nx::fs::open_file(cert_path, nx::fs::FileOpenOption::Read())?;
            let size = file.get_size()?;

            if size > 1_000_000 {
                tx.tx(format!(
                    "Error: Certificate {} bytes too large",
                    size - 1_000_000
                ));
                return Ok(None);
            }

            let mut buffer = alloc::vec![0u8; size];
            file.read_array(&mut buffer)?;
            buffer
        };

        Ok(Some((ticket_content, certificate_content)))
    }
}

fn copy_nca_to_placeholder(
    tx: &LoggerTx,
    path: String,
    content_storage: Shared<ContentStorage>,
    nca_id: [u8; 0x10],
) -> Result<()> {
    let mut nca_file = nx::fs::open_file(path.clone(), nx::fs::FileOpenOption::Read())?;

    let total_size = nca_file.get_size()?;
    let mut remaining_size = total_size;
    let mut offset = 0u64;
    let mut vec = alloc::vec![0u8; 64_000_000];
    let threshold_offset = total_size / 40;
    let mut next_threshold = threshold_offset;

    tx.tx(format!(
        "Installing {} - {}% {}/{}",
        path.as_str().split('/').last().unwrap_or(path.as_str()),
        0,
        0,
        total_size
    ));

    while remaining_size != 0 {
        let iter_size = remaining_size.min(64_000_000);

        let write_buffer = &mut vec[..iter_size];

        nca_file.read_array(write_buffer)?;
        content_storage.get().write_placeholder(
            PlaceholderId::new(nca_id),
            offset,
            nx::ipc::sf::InMapAliasBuffer::from_mut_array(write_buffer),
        )?;

        offset += iter_size as u64;
        remaining_size -= iter_size;

        if offset >= next_threshold as u64 {
            let percent = ((offset * 100) / total_size as u64).min(100).max(0);
            tx.edit_last(format!(
                "Installing {} - {}% {}/{}",
                path.as_str().split('/').last().unwrap_or(path.as_str()),
                percent,
                offset,
                total_size
            ));

            while offset >= next_threshold as u64 {
                next_threshold += threshold_offset;
            }
        }
    }

    Ok(())
}
