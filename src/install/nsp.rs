use alloc::{format, string::String, vec};
use nx::{
    fs::{FileReadOption, FileSystem},
    mem::Shared,
    result::Result,
};

use crate::{
    hos::{
        fs::nsp::NspFileSystem,
        hipc::ncm::{get_ncm_session, ContentId, IContentStorage, PlaceholderId, StorageId},
        parse_nca_id,
    },
    ui::logger_task::LoggerTx,
};

use super::{install_registered_cnmt, InstallSuccess, Installer};

#[derive(Clone)]
pub struct SdNspInstaller {
    nsp_fs: Shared<NspFileSystem>,
}

impl SdNspInstaller {
    pub fn new(content_path: String) -> Result<Self> {
        let nsp_fs = {
            let mut res = NspFileSystem::mount(content_path.clone());

            if res.is_err() {
                res = NspFileSystem::mount(content_path);
            }

            Shared::new(res?)
        };

        Ok(Self { nsp_fs })
    }
}

impl Installer for SdNspInstaller {
    fn install_nca(
        &self,
        tx: &LoggerTx,
        nca_path: String,
        storage_id: StorageId,
    ) -> Result<Option<[u8; 0x10]>> {
        let nca_id = nca_path
            .split('/')
            .last()
            .unwrap_or(nca_path.as_str())
            .trim_end_matches(".nca")
            .trim_end_matches(".cnmt");
        let nca_id = match parse_nca_id(tx, nca_id) {
            Some(nca_id) => nca_id,
            None => {
                tx.tx("Error install NCA: Couldn't parse NCA ID");
                return Ok(None);
            }
        };

        let content_storage = get_ncm_session()?.get().open_content_storage(storage_id)?;

        if let Ok(true) = content_storage.get().has(ContentId::new(nca_id)) {
            tx.tx("Content already registered!");
            return Ok(Some(nca_id));
        }

        let _ = content_storage
            .get()
            .delete_placeholder(PlaceholderId::new(nca_id));

        let nca_size = {
            let nca_path = match self.nsp_fs.get().get_nca_path_by_id(nca_id)? {
                Some(path) => path,
                None => return Ok(None),
            };
            let nca_file = self
                .nsp_fs
                .get()
                .open_file(nca_path, nx::fs::FileOpenMode::Read())?;
            nca_file.get().get_size()? as i64
        };

        content_storage.get().create_placeholder(
            ContentId::new(nca_id),
            PlaceholderId::new(nca_id),
            nca_size,
        )?;

        let res = self
            .nsp_fs
            .get()
            .copy_nca_to_placeholder(tx, nca_id, content_storage.clone());
        if let Err(why) = res {
            tx.tx(format!("Error: {}", why));
        }

        if let Err(why) = content_storage
            .get()
            .register(ContentId::new(nca_id), PlaceholderId::new(nca_id))
        {
            tx.tx(format!(
                "Failed to register placeholder ({}), deleting...",
                why
            ));
            content_storage
                .get()
                .delete_placeholder(PlaceholderId::new(nca_id))?;
            tx.tx("Deleted placeholder");
            return Ok(None);
        }

        Ok(Some(nca_id))
    }

    fn fetch_ticket_cert(&self, tx: &LoggerTx) -> Result<Option<(vec::Vec<u8>, vec::Vec<u8>)>> {
        let ticket_content = {
            let ticket_path = match self.nsp_fs.get().get_files_by_ext(".tik")?.get(0) {
                Some(ticket) => format!("/{}", ticket),
                None => {
                    tx.tx("Error: NCA contains no ticket...");
                    return Ok(None);
                }
            };

            let file = self
                .nsp_fs
                .get()
                .open_file(ticket_path, nx::fs::FileOpenMode::Read())?;
            let size = file.get().get_size()?;

            if size > 1_000_000 {
                tx.tx(format!(
                    "Error: Ticket {} bytes too large",
                    size - 1_000_000
                ));
                return Ok(None);
            }

            let mut buffer = vec![0u8; size];
            file.get()
                .read(0, buffer.as_mut_ptr(), buffer.len(), FileReadOption::None())?;
            buffer
        };

        let certificate_content = {
            let cert_path = match self.nsp_fs.get().get_files_by_ext(".cert")?.get(0) {
                Some(ticket) => format!("/{}", ticket),
                None => {
                    tx.tx("Error: NCA contains no certificate...");
                    return Ok(None);
                }
            };

            let file = self
                .nsp_fs
                .get()
                .open_file(cert_path, nx::fs::FileOpenMode::Read())?;
            let size = file.get().get_size()?;

            if size > 1_000_000 {
                tx.tx(format!(
                    "Error: Certificate {} bytes too large",
                    size - 1_000_000
                ));
                return Ok(None);
            }

            let mut buffer = vec![0u8; size];
            file.get()
                .read(0, buffer.as_mut_ptr(), buffer.len(), FileReadOption::None())?;
            buffer
        };

        Ok(Some((ticket_content, certificate_content)))
    }

    fn install(&self, tx: &LoggerTx, storage_id: StorageId) -> Result<Option<InstallSuccess>> {
        let cnmt = match self.nsp_fs.get().get_cnmt_name()? {
            Some(cnmt) => cnmt,
            None => {
                tx.tx("Error: Missing CNMT");
                return Ok(None);
            }
        };

        let cnmt_id = match self.install_nca(tx, format!("/{}", cnmt), storage_id)? {
            Some(id) => id,
            None => {
                tx.tx("Error: Failed to install CNMT");
                return Ok(None);
            }
        };

        let content_info_list = match install_registered_cnmt(tx, cnmt_id, storage_id)? {
            Some(content_info_list) => content_info_list,
            None => {
                tx.tx("Failed to read CNMT");
                return Ok(None);
            }
        };

        let mut ticket_cert_count = 1;
        if let Err(why) = self.install_ticket_cert(tx) {
            tx.tx(format!(
                "Failed to install ticket/cert ({}), continuing anyways",
                why
            ));
            ticket_cert_count = 0;
        }

        let mut installed = vec![cnmt_id];
        for content_info in content_info_list {
            let path = match self
                .nsp_fs
                .get()
                .get_nca_path_by_id(content_info.content_id.buffer)?
            {
                Some(path) => path,
                None => {
                    tx.tx(format!(
                        "NSP missing NCA {}, exiting...",
                        hex::encode(content_info.content_id.buffer)
                    ));

                    let content_storage =
                        get_ncm_session()?.get().open_content_storage(storage_id)?;

                    for installed in installed {
                        content_storage.get().delete(ContentId::new(installed))?;
                    }

                    return Ok(None);
                }
            };

            match self.install_nca(tx, path, storage_id)? {
                Some(nca_id) => {
                    installed.push(nca_id);
                }
                None => {
                    tx.tx(format!(
                        "Installation of NCA {} failed\nUninstalling and exiting...",
                        hex::encode(content_info.content_id.buffer)
                    ));

                    let content_storage =
                        get_ncm_session()?.get().open_content_storage(storage_id)?;

                    for installed in installed {
                        content_storage.get().delete(ContentId::new(installed))?;
                    }

                    return Ok(None);
                }
            };
        }

        Ok(Some(InstallSuccess::new(
            installed.len(),
            ticket_cert_count,
            ticket_cert_count,
        )))
    }
}
