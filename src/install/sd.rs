use alloc::{format, string::String};
use nx::result::Result;

use crate::{hos::hipc::ncm::StorageId, ui::logger_task::LoggerTx};

use super::{nca::SdNcaInstaller, nsp::SdNspInstaller, Installer};

fn content_path(path: &str) -> String {
    format!("@Sdcard{}", path.trim_start_matches("sdmc"))
}

pub fn handle_sd_install_nsp(path: &str, storage_id: StorageId, tx: LoggerTx) -> Result<()> {
    tx.tx(format!("Installing {}\nDestination: {}", path, storage_id));

    if let Some(success) = SdNspInstaller::new(content_path(path))?.install(&tx, storage_id)? {
        tx.tx(format!(
            "Success, installed {} NCA{}, {} ticket, and {} certificate!",
            success.nca_count,
            if success.nca_count > 1 { "s" } else { "" },
            success.ticket_count,
            success.certificate_count
        ));
    }

    Ok(())
}

pub fn handle_sd_install_unpacked_nca(
    path: String,
    storage_id: StorageId,
    tx: LoggerTx,
) -> Result<()> {
    tx.tx(format!("Installing NCAs from {} to {}", path, storage_id));

    let installer = match SdNcaInstaller::new(&tx, path)? {
        Some(installer) => installer,
        None => {
            tx.tx("Error: Missing CNMT");
            return Ok(());
        }
    };

    if let Some(success) = installer.install(&tx, storage_id)? {
        tx.tx(format!(
            "Success, installed {} NCA{}, {} ticket, and {} certificate!",
            success.nca_count,
            if success.nca_count > 1 { "s" } else { "" },
            success.ticket_count,
            success.certificate_count
        ));
    }

    Ok(())
}
