#![no_std]
#![no_main]

pub mod backup;
pub mod hos;
pub mod install;
pub mod ui;

extern crate alloc;
extern crate nx;

use alloc::{boxed::Box, format, rc::Rc, string::String};
use hos::hipc::{
    es::{finalise_es_session, initialise_es_session},
    ncm::{finalise_ncm_session, initialise_ncm_session},
    ns::{finalise_ns_session, initialise_ns_session},
};
use meow_ui::{component::text::Text, gui::Gui, scene::menu::Menu, themes::Theme};
use nx::{
    diag::{abort, log},
    gpu,
    mem::Shared,
    result::*,
    svc, util,
};
use ui::{
    logger_task::channel, strings::TITLE_MAIN_MENU, worker::worker_channel, InstallerMenu,
    MenuState,
};

use core::panic;

#[no_mangle]
pub fn initialize_heap(hbl_heap: util::PointerAndSize) -> util::PointerAndSize {
    if hbl_heap.is_valid() {
        hbl_heap
    } else {
        let heap_size: usize = 0x10000000;
        let heap_address = svc::set_heap_size(heap_size).unwrap();
        util::PointerAndSize::new(heap_address, heap_size)
    }
}

fn setup() -> Result<()> {
    nx::fs::initialize_fspsrv_session()?;
    nx::fs::mount_sd_card("sdmc")?;
    match nx::fs::get_entry_type(String::from("sdmc:/meow")) {
        Ok(_) => {
            if nx::fs::get_entry_type(String::from("sdmc:/meow/installer")).is_err() {
                nx::fs::create_directory(String::from("sdmc:/meow/installer"))?;
            }
        }
        Err(_) => {
            nx::fs::create_directory(String::from("sdmc:/meow"))?;
            nx::fs::create_directory(String::from("sdmc:/meow/installer"))?;
        }
    }
    initialise_es_session(nx::service::new_service_object::<
        hos::hipc::es::ETicketService,
    >()?);
    initialise_ncm_session(nx::service::new_service_object::<
        hos::hipc::ncm::ContentManager,
    >()?);
    initialise_ns_session(nx::service::new_service_object::<
        hos::hipc::ns::ServiceGetterInterface,
    >()?)?;

    Ok(())
}

fn finalise() {
    finalise_ns_session();
    finalise_ncm_session();
    finalise_es_session();

    nx::fs::finalize_fspsrv_session();
    nx::fs::unmount_all();
}

#[no_mangle]
pub fn main() -> Result<()> {
    setup()?;

    let mut gpu_ctx = gpu::Context::new(
        gpu::NvDrvServiceKind::Applet,
        gpu::ViServiceKind::Manager,
        0x40000,
    )?;

    let mut gui = Gui::new(
        Theme::DARK,
        gpu_ctx.create_stray_layer_surface(
            "Default",
            2,
            gpu::ColorFormat::A8B8G8R8,
            gpu::PixelFormat::RGBA_8888,
            gpu::Layout::BlockLinear,
        )?,
    )?;

    let title = Shared::new(Text::new(
        TITLE_MAIN_MENU,
        0,
        0,
        meow_ui::component::text::TextSize::Medium,
        false,
    ));
    let state = Shared::new(ui::MenuState::MainMenu);

    let (logger_tx, logger_rx) = channel();
    let (i_ch, channels) = worker_channel(logger_tx, logger_rx);

    let boxed_lock = Box::new(nx::sync::Locked::new(false, i_ch));

    let mut thread = nx::thread::Thread::new(
        |locked| {
            let y = locked.get();

            if let Err(why) = ui::worker::worker_main(y) {
                y.tx.tx(format!("Error {} from worker", why));
            }

            nx::thread::exit();
        },
        &(boxed_lock.as_ref()),
        "bgLoggerTask",
        0x10000,
    )?;

    thread.initialize(
        nx::thread::PRIORITY_AUTO,
        nx::svc::DEFAULT_PROCESS_PROCESSOR_ID,
    )?;
    thread.start()?;

    let channels = Rc::new(channels);

    let menu_scene = Menu::new_shared_title(
        title.clone(),
        Shared::new(InstallerMenu::new(title, state.clone(), channels.clone())?),
    );

    gui.push_scene(menu_scene);

    while *state.get() != MenuState::Exit {
        gui.present()?;
        gui.poll_events();

        // Sleep 10ms (aka 10'000'000 ns)
        svc::sleep_thread(10_000_000)?;
    }

    channels.tx.tx(ui::worker::WorkerCommand::Exit);
    finalise();

    Ok(())
}

#[panic_handler]
fn panic_handler(info: &panic::PanicInfo) -> ! {
    util::simple_panic_handler::<log::lm::LmLogger>(info, abort::AbortLevel::FatalThrow())
}
