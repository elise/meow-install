pub mod backup;
pub mod credits;
pub mod install;
pub mod logger_task;
pub mod main_menu;
pub mod strings;
pub mod worker;

use alloc::{rc::Rc, string::String};
use meow_ui::component::{
    text::{Text, TextSize},
    Component,
};
use nx::{mem::Shared, result::Result};

use crate::hos::hipc::ncm::ProgramId;

use self::{
    backup::{
        format::BackupFormatSelection, source::BackupSelectSourceMenu,
        sys_title_selection::TitleSelection,
    },
    credits::Credits,
    install::destination::InstallationDestinationMenu,
    install::file_menu::{FileMenu, FileMenuSearchConfiguration},
    install::{destination::StorageLocation, sd_installation_menu::SdInstallationMenu},
    logger_task::LoggerTask,
    main_menu::MainMenu,
    strings::{
        TITLE_DEST_SEL, TITLE_MAIN_MENU, TITLE_SD_INSTALL, TITLE_SD_NCA_BACKUP,
        TITLE_SD_NCA_INSTALL, TITLE_SD_NSP_BACKUP, TITLE_SD_NSP_INSTALL,
    },
    worker::WorkerChannelPair,
};

#[derive(Clone, PartialEq, Eq)]
pub enum InstallSource {
    SdNsp(String),
    SdUnpackedNsp(String),
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum BackupType {
    SysToSd(String, ProgramId, StorageLocation),
}

#[derive(Clone, PartialEq, Eq)]
pub enum MenuState {
    MainMenu,
    Credits,
    BackupToSDSourceSelection,
    BackupToSDTitleSelection,
    BackupToSDOutFormatSelection(BackupType),
    InstallFromSDMenu,
    InstallFromSDNSPSelection,
    InstallFromSDUnpackedNSPSelection,
    InstallChooseDestination(InstallSource),
    Logger,
    Exit,
}

pub struct InstallerMenu {
    pub text: Shared<Text>,
    pub state: Shared<MenuState>,

    main_menu: Shared<MainMenu>,
    credits: Shared<Credits>,

    backup_source_select: Shared<BackupSelectSourceMenu>,
    backup_title_select: Shared<TitleSelection>,
    backup_dest_select: Shared<BackupFormatSelection>,

    sd_installation_menu: Shared<SdInstallationMenu>,
    sd_installation_nsp_file_select: Shared<FileMenu>,
    sd_installation_unpacked_nca_file_select: Shared<FileMenu>,
    installation_destination_selection: Shared<InstallationDestinationMenu>,

    logger: Shared<LoggerTask>,
}

impl InstallerMenu {
    pub fn new(
        text: Shared<Text>,
        state: Shared<MenuState>,
        ch: Rc<WorkerChannelPair>,
    ) -> Result<Self> {
        let sd_installation_nsp_file_select = {
            let text = text.clone();
            let text2 = text.clone();
            let state = state.clone();
            let state2 = state.clone();
            Shared::new(
                FileMenu::new(FileMenuSearchConfiguration::Extension("nsp"))?
                    .on_back(move || {
                        text.get().set_content(TITLE_SD_INSTALL);
                        *state.get() = MenuState::InstallFromSDMenu;
                    })
                    .on_file_chosen(move |file| {
                        text2.get().set_content(TITLE_DEST_SEL);
                        *state2.get() =
                            MenuState::InstallChooseDestination(InstallSource::SdNsp(file.path))
                    })?,
            )
        };
        let sd_installation_unpacked_nsp_file_select = {
            let text = text.clone();
            let text2 = text.clone();
            let state = state.clone();
            let state2 = state.clone();
            Shared::new(
                FileMenu::new(FileMenuSearchConfiguration::Folders)?
                    .on_back(move || {
                        text.get().set_content(TITLE_SD_INSTALL);
                        *state.get() = MenuState::InstallFromSDMenu;
                    })
                    .on_file_chosen(move |file| {
                        text2.get().set_content(TITLE_DEST_SEL);
                        *state2.get() = MenuState::InstallChooseDestination(
                            InstallSource::SdUnpackedNsp(file.path),
                        )
                    })?,
            )
        };

        let main_menu = Shared::new(MainMenu::new(text.clone(), state.clone()));
        let credits = Shared::new(Credits::new(text.clone(), state.clone()));
        let sd_installation_menu = Shared::new(SdInstallationMenu::new(
            text.clone(),
            state.clone(),
            sd_installation_nsp_file_select.clone(),
            sd_installation_unpacked_nsp_file_select.clone(),
        ));
        let installation_destination_selection = Shared::new({
            let text = text.clone();
            let text1 = text.clone();
            let state = state.clone();
            let state1 = state.clone();
            let ch = ch.clone();

            InstallationDestinationMenu::new(state, text, move |destination| {
                let install_source = match state1.get() {
                    MenuState::InstallChooseDestination(path) => path.clone(),
                    _ => {
                        text1.get().set_content(TITLE_MAIN_MENU);
                        *state1.get() = MenuState::MainMenu;
                        return;
                    }
                };

                *state1.get() = MenuState::Logger;

                match install_source {
                    InstallSource::SdNsp(path) => {
                        text1.get().set_content(TITLE_SD_NSP_INSTALL);

                        ch.tx
                            .tx(worker::WorkerCommand::SDInstallNSP(path, destination))
                    }
                    InstallSource::SdUnpackedNsp(path) => {
                        text1.get().set_content(TITLE_SD_NCA_INSTALL);

                        ch.tx
                            .tx(worker::WorkerCommand::SDInstallUnpacked(path, destination))
                    }
                }
            })
        });

        let backup_title_select = Shared::new(TitleSelection::new(state.clone())?);

        let backup_source_select = Shared::new({
            let text = text.clone();
            let state = state.clone();
            let state1 = state.clone();
            let backup_title_select = backup_title_select.clone();

            BackupSelectSourceMenu::new(state, text, move |source| match source {
                backup::source::BackupSource::GameCard => {}
                backup::source::BackupSource::System => {
                    *state1.get() = MenuState::BackupToSDTitleSelection;
                    let _ = backup_title_select.get().refresh();
                }
            })
        });

        let backup_dest_select = Shared::new({
            let text = text.clone();
            let state = state.clone();
            let state1 = state.clone();
            let ch = ch.clone();

            BackupFormatSelection::new(state, move |format| {
                let (name, id, location) = match state1.get() {
                    MenuState::BackupToSDOutFormatSelection(BackupType::SysToSd(
                        name,
                        id,
                        location,
                    )) => (name.clone(), *id, *location),
                    _ => {
                        text.get().set_content(TITLE_MAIN_MENU);
                        *state1.get() = MenuState::MainMenu;
                        return;
                    }
                };

                *state1.get() = MenuState::Logger;

                match format {
                    backup::format::BackupFormat::Nsp => {
                        text.get().set_content(TITLE_SD_NSP_BACKUP);

                        ch.tx.tx(worker::WorkerCommand::SDBackupNSPConsole(
                            name, id, location,
                        ))
                    }
                    backup::format::BackupFormat::UnpackedNsp => {
                        text.get().set_content(TITLE_SD_NCA_BACKUP);

                        ch.tx.tx(worker::WorkerCommand::SDBackupUnpackedNSPConsole(
                            name, id, location,
                        ))
                    }
                }
            })
        });

        let logger = {
            let text = text.clone();
            let state = state.clone();
            Shared::new(LoggerTask::new(
                ch,
                move || {},
                move || {
                    text.get().set_content(TITLE_MAIN_MENU);
                    *state.get() = MenuState::MainMenu
                },
                19,
            ))
        };

        Ok(Self {
            text,
            state,
            main_menu,
            credits,
            sd_installation_menu,
            sd_installation_nsp_file_select,
            sd_installation_unpacked_nca_file_select: sd_installation_unpacked_nsp_file_select,
            logger,
            installation_destination_selection,
            backup_source_select,
            backup_title_select,
            backup_dest_select,
        })
    }
}

impl Component for InstallerMenu {
    fn x(&self) -> meow_ui::component::ComponentCoordiate {
        0
    }

    fn y(&self) -> meow_ui::component::ComponentCoordiate {
        0
    }

    fn draw_frame(
        &mut self,
        parent: (
            meow_ui::component::ComponentCoordiate,
            meow_ui::component::ComponentCoordiate,
        ),
        draw: &mut meow_ui::draw::Draw,
        theme: &meow_ui::themes::Theme,
    ) -> (
        meow_ui::component::ComponentSize,
        meow_ui::component::ComponentSize,
    ) {
        match self.state.get() {
            MenuState::MainMenu => self.main_menu.get().draw_frame(parent, draw, theme),
            MenuState::Credits => self.credits.get().draw_frame(parent, draw, theme),
            MenuState::InstallFromSDMenu => self
                .sd_installation_menu
                .get()
                .draw_frame(parent, draw, theme),
            MenuState::InstallFromSDUnpackedNSPSelection => self
                .sd_installation_unpacked_nca_file_select
                .get()
                .draw_frame(parent, draw, theme),
            MenuState::Exit => Text::new(
                String::from("Exiting..."),
                456,
                256,
                TextSize::Medium,
                false,
            )
            .draw_frame(parent, draw, theme),
            MenuState::InstallChooseDestination(_) => self
                .installation_destination_selection
                .get()
                .draw_frame(parent, draw, theme),
            MenuState::Logger => self.logger.get().draw_frame(parent, draw, theme),
            MenuState::InstallFromSDNSPSelection => self
                .sd_installation_nsp_file_select
                .get()
                .draw_frame(parent, draw, theme),
            MenuState::BackupToSDSourceSelection => self
                .backup_source_select
                .get()
                .draw_frame(parent, draw, theme),
            MenuState::BackupToSDTitleSelection => self
                .backup_title_select
                .get()
                .draw_frame(parent, draw, theme),
            MenuState::BackupToSDOutFormatSelection(_) => self
                .backup_dest_select
                .get()
                .draw_frame(parent, draw, theme),
        }
    }

    fn handle_event(&mut self, event: meow_ui::input::InputEvent) {
        match self.state.get() {
            MenuState::Credits => self.credits.get().handle_event(event),
            MenuState::MainMenu => self.main_menu.get().handle_event(event),
            MenuState::InstallFromSDMenu => self.sd_installation_menu.get().handle_event(event),
            MenuState::InstallFromSDUnpackedNSPSelection => self
                .sd_installation_unpacked_nca_file_select
                .get()
                .handle_event(event),
            MenuState::Exit => {}
            MenuState::Logger => self.logger.get().handle_event(event),
            MenuState::InstallFromSDNSPSelection => self
                .sd_installation_nsp_file_select
                .get()
                .handle_event(event),
            MenuState::InstallChooseDestination(_) => self
                .installation_destination_selection
                .get()
                .handle_event(event),
            MenuState::BackupToSDSourceSelection => {
                self.backup_source_select.get().handle_event(event)
            }
            MenuState::BackupToSDTitleSelection => {
                self.backup_title_select.get().handle_event(event)
            }
            MenuState::BackupToSDOutFormatSelection(_) => {
                self.backup_dest_select.get().handle_event(event)
            }
        }
    }
}
