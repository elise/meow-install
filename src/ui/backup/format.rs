use alloc::rc::Rc;
use meow_ui::{
    component::{button::TextButton, list::List, rectangle::FilledRectangle, Component},
    input::InputEvent,
    list,
};
use nx::mem::Shared;

use crate::ui::MenuState;

#[derive(Debug, Clone, Copy)]
pub enum BackupFormat {
    Nsp,
    UnpackedNsp,
}

pub struct BackupFormatSelection {
    list: List,

    state: Shared<MenuState>,
}

impl BackupFormatSelection {
    pub fn new<T: Fn(BackupFormat) + 'static>(state: Shared<MenuState>, on_selected: T) -> Self {
        let state1 = state.clone();
        let on_selected_1 = Rc::new(on_selected);
        let on_selected_2 = on_selected_1.clone();

        Self {
            list: list![
                Shared::new(TextButton::new("NSP", 0, 0).on_click(move || {
                    on_selected_1(BackupFormat::Nsp);
                })),
                Shared::new(TextButton::new("Unpacked NSP", 0, 0).on_click(move || {
                    on_selected_2(BackupFormat::UnpackedNsp);
                })),
                Shared::new(FilledRectangle::new(0, 16, 64, 2, true)),
                Shared::new(TextButton::new("Back", 0, 0).on_click(move || {
                    *state1.get() = MenuState::BackupToSDTitleSelection;
                }))
            ],
            state,
        }
    }
}

impl Component for BackupFormatSelection {
    fn x(&self) -> meow_ui::component::ComponentCoordiate {
        self.list.x()
    }

    fn y(&self) -> meow_ui::component::ComponentCoordiate {
        self.list.y()
    }

    fn draw_frame(
        &mut self,
        parent: (
            meow_ui::component::ComponentCoordiate,
            meow_ui::component::ComponentCoordiate,
        ),
        draw: &mut meow_ui::draw::Draw,
        theme: &meow_ui::themes::Theme,
    ) -> (
        meow_ui::component::ComponentSize,
        meow_ui::component::ComponentSize,
    ) {
        self.list.draw_frame(parent, draw, theme)
    }

    fn handle_event(&mut self, event: InputEvent) {
        if event == InputEvent::B {
            *self.state.get() = MenuState::BackupToSDTitleSelection;
        } else {
            self.list.handle_event(event)
        }
    }
}
