use core::ffi::CStr;

use alloc::{boxed::Box, format, string::ToString, vec::Vec};
use meow_ui::{
    component::{button::TextButton, list::List, Component},
    input::InputEvent,
};
use nx::{
    ipc::sf::{Buffer, EnumAsPrimitiveType},
    mem::Shared,
    result::Result,
};

use crate::{
    hos::hipc::{
        ncm::{ContentMetaType, ProgramId},
        ns::{
            get_ns_am_session, ApplicationContentMetaStatus, ApplicationControlData,
            ApplicationControlSource, ApplicationRecord,
        },
    },
    ui::{install::destination::StorageLocation, MenuState},
};

pub struct TitleSelection {
    state: Shared<MenuState>,
    list: List,
}

impl TitleSelection {
    pub fn refresh(&mut self) -> Result<()> {
        let count = get_ns_am_session()?
            .get()
            .generate_application_record_count()? as usize;
        let mut record_list = Vec::with_capacity(count);
        record_list.resize(count, ApplicationRecord::default());
        get_ns_am_session()?
            .get()
            .list_application_records(Buffer::from_mut_array(&mut record_list), 0)?;

        let mut nacp_data: Box<ApplicationControlData> = Box::default();

        let mut processed_records = Vec::new();

        for record in record_list {
            match record.application_id.0 != 0 {
                true => {
                    get_ns_am_session()?.get().get_application_control_data(
                        EnumAsPrimitiveType::from(ApplicationControlSource::Storage),
                        record.application_id,
                        Buffer::from_mut_ptr(nacp_data.as_mut(), 1),
                    )?;

                    let title_name = if nacp_data.nacp.titles[1].application_name[0] != 0 {
                        CStr::from_bytes_until_nul(&nacp_data.nacp.titles[1].application_name)
                            .unwrap()
                            .to_string_lossy()
                            .as_ref()
                            .to_string()
                    } else if nacp_data.nacp.titles[0].application_name[0] != 0 {
                        CStr::from_bytes_until_nul(&nacp_data.nacp.titles[0].application_name)
                            .unwrap()
                            .to_string_lossy()
                            .as_ref()
                            .to_string()
                    } else {
                        format!("{:016X}", record.application_id.0)
                    };

                    let mut buffer = alloc::vec![ApplicationContentMetaStatus::default(); 0x80];
                    let entry_count = get_ns_am_session()?
                        .get()
                        .list_application_content_meta_status(
                            Buffer::from_mut_array(&mut buffer),
                            0,
                            record.application_id,
                        )?;
                    let mut storage_location = None;

                    'inner: for entry in buffer.iter().take(entry_count.min(0x80) as usize) {
                        if entry.content_meta_type == ContentMetaType::Application {
                            if entry.storage_id == crate::hos::hipc::ncm::StorageId::BuiltInUser {
                                storage_location = Some(StorageLocation::Nand);
                                break 'inner;
                            } else if entry.storage_id == crate::hos::hipc::ncm::StorageId::SdCard {
                                storage_location = Some(StorageLocation::SdCard);
                                break 'inner;
                            }
                        }
                    }

                    if let Some(storage_location) = storage_location {
                        processed_records.push((
                            title_name,
                            record.application_id.0,
                            storage_location,
                        ));
                    }

                    unsafe {
                        core::ptr::write_bytes(nacp_data.as_mut(), 0, 1);
                    }
                }
                false => {}
            }
        }

        let mut list = List::new(0, 0, 10);

        for (name, id, location) in processed_records {
            let state = self.state.clone();
            list = list.child(TextButton::new(name.as_str(), 0, 0).on_click(move || {
                *state.get() = MenuState::BackupToSDOutFormatSelection(
                    crate::ui::BackupType::SysToSd(name.clone(), ProgramId(id), location),
                );
            }));
        }

        if !list.children.is_empty() {
            list = list.selected_idx(0);
        }

        self.list = list;
        Ok(())
    }

    pub fn new(state: Shared<MenuState>) -> Result<Self> {
        let mut out = Self {
            state,
            list: List::new(0, 0, 0),
        };

        out.refresh()?;

        Ok(out)
    }
}

impl Component for TitleSelection {
    fn x(&self) -> meow_ui::component::ComponentCoordiate {
        self.list.x()
    }

    fn y(&self) -> meow_ui::component::ComponentCoordiate {
        self.list.y()
    }

    fn draw_frame(
        &mut self,
        parent: (
            meow_ui::component::ComponentCoordiate,
            meow_ui::component::ComponentCoordiate,
        ),
        draw: &mut meow_ui::draw::Draw,
        theme: &meow_ui::themes::Theme,
    ) -> (
        meow_ui::component::ComponentSize,
        meow_ui::component::ComponentSize,
    ) {
        self.list.draw_frame(parent, draw, theme)
    }

    fn handle_event(&mut self, event: InputEvent) {
        if event == InputEvent::B {
            *self.state.get() = MenuState::BackupToSDSourceSelection;
        } else {
            self.list.handle_event(event)
        }
    }
}
