use meow_ui::{
    component::{
        rectangle::FilledRectangle,
        stack::VStack,
        text::{Text, TextSize},
        Component,
    },
    input::InputEvent,
    vstack,
};
use nx::mem::Shared;

use super::{strings::TITLE_MAIN_MENU, MenuState};

pub struct Credits {
    content: Shared<VStack>,

    menu_text: Shared<Text>,
    menu_state: Shared<MenuState>,
}

impl Credits {
    pub fn new(menu_text: Shared<Text>, menu_state: Shared<MenuState>) -> Self {
        Self {
            content: vstack![
                Shared::new(Text::new("Main Development", 0, 0, TextSize::Medium, false)),
                Shared::new(FilledRectangle::new(2, 4, 298, 1, true)),
                Shared::new(meow_ui::component::padding::Padding(0, 16)),
                Shared::new(Text::new("EliseZeroTwo", 0, 0, TextSize::Small, false)),
                Shared::new(Text::new(
                    "\nMiscellaneous Development",
                    0,
                    0,
                    TextSize::Medium,
                    false
                )),
                Shared::new(FilledRectangle::new(2, 4, 452, 1, true)),
                Shared::new(meow_ui::component::padding::Padding(0, 16)),
                Shared::new(Text::new(
                    "aarch64-switch-rs - Rust Toolchain",
                    0,
                    0,
                    TextSize::Small,
                    false
                ))
            ],
            menu_text,
            menu_state,
        }
    }
}

impl Component for Credits {
    fn x(&self) -> meow_ui::component::ComponentCoordiate {
        0
    }

    fn y(&self) -> meow_ui::component::ComponentCoordiate {
        0
    }

    fn draw_frame(
        &mut self,
        parent: (
            meow_ui::component::ComponentCoordiate,
            meow_ui::component::ComponentCoordiate,
        ),
        draw: &mut meow_ui::draw::Draw,
        theme: &meow_ui::themes::Theme,
    ) -> (
        meow_ui::component::ComponentSize,
        meow_ui::component::ComponentSize,
    ) {
        self.content.get().draw_frame(parent, draw, theme)
    }

    fn handle_event(&mut self, event: meow_ui::input::InputEvent) {
        if event == InputEvent::B {
            self.menu_text.get().set_content(TITLE_MAIN_MENU);
            *self.menu_state.get() = MenuState::MainMenu;
        }
    }

    fn selectable(&self) -> bool {
        true
    }
}
