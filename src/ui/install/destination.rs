use meow_ui::{
    component::{
        button::TextButton, list::List, rectangle::FilledRectangle, text::Text, Component,
    },
    input::InputEvent,
    list,
};
use nx::mem::Shared;

use crate::hos::hipc::ncm::StorageId;

use crate::ui::{strings::TITLE_MAIN_MENU, MenuState};

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum StorageLocation {
    SdCard,
    Nand,
}

impl From<StorageLocation> for StorageId {
    fn from(value: StorageLocation) -> Self {
        match value {
            StorageLocation::SdCard => Self::SdCard,
            StorageLocation::Nand => Self::BuiltInUser,
        }
    }
}

pub struct InstallationDestinationMenu {
    state: Shared<MenuState>,
    text: Shared<Text>,
    list: List,
}

impl InstallationDestinationMenu {
    pub fn new<T: Fn(StorageLocation) + 'static>(
        state: Shared<MenuState>,
        text: Shared<Text>,
        on_selected: T,
    ) -> Self {
        let on_selected_1 = alloc::rc::Rc::new(on_selected);
        let on_selected_2 = on_selected_1.clone();
        let state1 = state.clone();
        let text1 = text.clone();
        Self {
            state,
            text,
            list: list![
                Shared::new(TextButton::new("SD Card", 0, 0).on_click(move || {
                    on_selected_1(StorageLocation::SdCard);
                })),
                Shared::new(TextButton::new("NAND", 0, 0).on_click(move || {
                    on_selected_2(StorageLocation::Nand);
                })),
                Shared::new(FilledRectangle::new(0, 16, 64, 2, true)),
                Shared::new(TextButton::new("Back", 0, 0).on_click(move || {
                    *state1.get() = MenuState::MainMenu;
                    text1.get().set_content(TITLE_MAIN_MENU);
                }))
            ],
        }
    }
}

impl Component for InstallationDestinationMenu {
    fn x(&self) -> meow_ui::component::ComponentCoordiate {
        self.list.x()
    }

    fn y(&self) -> meow_ui::component::ComponentCoordiate {
        self.list.y()
    }

    fn draw_frame(
        &mut self,
        parent: (
            meow_ui::component::ComponentCoordiate,
            meow_ui::component::ComponentCoordiate,
        ),
        draw: &mut meow_ui::draw::Draw,
        theme: &meow_ui::themes::Theme,
    ) -> (
        meow_ui::component::ComponentSize,
        meow_ui::component::ComponentSize,
    ) {
        self.list.draw_frame(parent, draw, theme)
    }

    fn handle_event(&mut self, event: InputEvent) {
        if event == InputEvent::B {
            *self.state.get() = MenuState::MainMenu;
            self.text.get().set_content(TITLE_MAIN_MENU);
        } else {
            self.list.handle_event(event)
        }
    }
}
