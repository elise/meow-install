use alloc::{format, rc::Rc, string::String, vec::Vec};
use meow_ui::{
    component::{
        button::TextButton, list::List, padding::Padding, text::Text, Callback, Component,
    },
    input::InputEvent,
    list, vstack,
};
use nx::{
    fs::{DirectoryEntryType, DirectoryOpenMode},
    mem::Shared,
    result::Result,
};

#[derive(Clone, Copy, PartialEq, Eq)]
pub enum FileMenuSearchConfiguration {
    Extension(&'static str),
    All,
    Folders,
}

pub struct FileMenu {
    config: FileMenuSearchConfiguration,
    path: Vec<String>,

    text: Shared<Text>,
    list: Shared<List>,

    on_file_chosen: Option<Rc<dyn Fn(FileEntry)>>,
    on_back: Option<Callback>,
}

#[derive(Clone)]
pub struct FileEntry {
    pub path: String,
    pub is_dir: bool,
}

impl FileMenu {
    pub fn new(config: FileMenuSearchConfiguration) -> Result<Self> {
        let mut out = Self {
            config,
            path: Vec::new(),
            text: Shared::new(Text::new(
                "Path: sdmc:/meow/installer/",
                0,
                0,
                meow_ui::component::text::TextSize::Medium,
                false,
            )),
            list: Shared::new(list![]),
            on_file_chosen: None,
            on_back: None,
        };

        out.update()?;

        Ok(out)
    }

    pub fn on_file_chosen<T: Fn(FileEntry) + 'static>(mut self, callback: T) -> Result<Self> {
        self.on_file_chosen = Some(Rc::new(callback));
        self.update()?;

        Ok(self)
    }

    pub fn on_back<T: Into<Callback> + 'static>(mut self, callback: T) -> Self {
        self.on_back = Some(callback.into());

        self
    }

    pub fn get_path(&self) -> String {
        format!("sdmc:/meow/installer/{}", self.path.join("/"))
    }

    pub fn update(&mut self) -> Result<()> {
        let path = self.get_path();

        self.text.get().set_content(path.clone());

        let mut dir = nx::fs::open_directory(
            path,
            match self.config {
                FileMenuSearchConfiguration::Extension(_) => DirectoryOpenMode::ReadFiles(),
                FileMenuSearchConfiguration::All => {
                    DirectoryOpenMode::ReadDirectories() | DirectoryOpenMode::ReadFiles()
                }
                FileMenuSearchConfiguration::Folders => DirectoryOpenMode::ReadDirectories(),
            },
        )?;

        let mut entries = Vec::new();

        while let Ok(Some(dd)) = dir.read_next() {
            entries.push(FileEntry {
                path: format!("sdmc:/meow/installer/{}", dd.name.get_string()?),
                is_dir: dd.entry_type == DirectoryEntryType::Directory,
            })
        }

        let mut list = List::new(0, 0, 9);

        for entry in &entries {
            let name = entry.path.split('/').last().unwrap();

            if let FileMenuSearchConfiguration::Extension(ext) = self.config {
                if !name.ends_with(&format!(".{}", ext)) {
                    continue;
                }
            }

            let on_file_chosen = self.on_file_chosen.clone();
            let entry = entry.clone();
            list = list.child(TextButton::new(name, 0, 0).on_click(move || {
                if let Some(handler) = on_file_chosen.as_ref() {
                    handler.as_ref()(entry.clone())
                }
            }));
        }

        if !list.children.is_empty() {
            list = list.selected_idx(0);
        }

        self.list = Shared::new(list);

        Ok(())
    }
}

impl Component for FileMenu {
    fn x(&self) -> meow_ui::component::ComponentCoordiate {
        0
    }

    fn y(&self) -> meow_ui::component::ComponentCoordiate {
        0
    }

    fn draw_frame(
        &mut self,
        parent: (
            meow_ui::component::ComponentCoordiate,
            meow_ui::component::ComponentCoordiate,
        ),
        draw: &mut meow_ui::draw::Draw,
        theme: &meow_ui::themes::Theme,
    ) -> (
        meow_ui::component::ComponentSize,
        meow_ui::component::ComponentSize,
    ) {
        vstack![
            self.text.clone(),
            Shared::new(Padding(0, 32)),
            self.list.clone()
        ]
        .get()
        .draw_frame(parent, draw, theme)
    }

    fn selectable(&self) -> bool {
        true
    }

    fn handle_event(&mut self, event: meow_ui::input::InputEvent) {
        if event == InputEvent::B {
            if let Some(callback) = self.on_back.as_ref() {
                callback.call();
            }
        } else {
            self.list.get().handle_event(event)
        }
    }
}
