use meow_ui::{
    component::{
        button::TextButton, list::List, rectangle::FilledRectangle, text::Text, Component,
    },
    input::InputEvent,
    list,
};
use nx::mem::Shared;

use crate::ui::{
    strings::{TITLE_MAIN_MENU, TITLE_SD_NCA_INSTALL, TITLE_SD_NSP_INSTALL},
    MenuState,
};

use super::file_menu::FileMenu;

pub struct SdInstallationMenu {
    list: List,

    menu_text: Shared<Text>,
    menu_state: Shared<MenuState>,
}

impl SdInstallationMenu {
    pub fn new(
        menu_text: Shared<Text>,
        menu_state: Shared<MenuState>,
        file_menu_nsp: Shared<FileMenu>,
        file_menu_unpacked_nsp: Shared<FileMenu>,
    ) -> Self {
        let menu_text_1 = menu_text.clone();
        let menu_text_2 = menu_text.clone();
        let menu_text_3 = menu_text.clone();

        let menu_state_1 = menu_state.clone();
        let menu_state_2 = menu_state.clone();
        let menu_state_3 = menu_state.clone();

        Self {
            list: list![
                Shared::new(TextButton::new("Install NSP", 0, 0).on_click(move || {
                    menu_text_1.get().set_content(TITLE_SD_NSP_INSTALL);
                    *menu_state_1.get() = MenuState::InstallFromSDNSPSelection;
                    let _ = file_menu_nsp.get().update();
                })),
                Shared::new(
                    TextButton::new("Install Unpacked NSP", 0, 0).on_click(move || {
                        menu_text_2.get().set_content(TITLE_SD_NCA_INSTALL);
                        *menu_state_2.get() = MenuState::InstallFromSDUnpackedNSPSelection;
                        let _ = file_menu_unpacked_nsp.get().update();
                    })
                ),
                Shared::new(FilledRectangle::new(0, 16, 64, 2, true)),
                Shared::new(TextButton::new("Back", 0, 0).on_click(move || {
                    menu_text_3.get().set_content(TITLE_MAIN_MENU);
                    *menu_state_3.get() = MenuState::MainMenu;
                }))
            ],
            menu_text,
            menu_state,
        }
    }
}

impl Component for SdInstallationMenu {
    fn x(&self) -> meow_ui::component::ComponentCoordiate {
        0
    }

    fn y(&self) -> meow_ui::component::ComponentCoordiate {
        0
    }

    fn draw_frame(
        &mut self,
        parent: (
            meow_ui::component::ComponentCoordiate,
            meow_ui::component::ComponentCoordiate,
        ),
        draw: &mut meow_ui::draw::Draw,
        theme: &meow_ui::themes::Theme,
    ) -> (
        meow_ui::component::ComponentSize,
        meow_ui::component::ComponentSize,
    ) {
        self.list.draw_frame(parent, draw, theme)
    }

    fn handle_event(&mut self, event: meow_ui::input::InputEvent) {
        if event == InputEvent::B {
            self.menu_text.get().set_content(TITLE_MAIN_MENU);
            *self.menu_state.get() = MenuState::MainMenu;
        } else {
            self.list.handle_event(event)
        }
    }

    fn selectable(&self) -> bool {
        true
    }
}
