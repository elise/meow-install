use alloc::{rc::Rc, vec::Vec};
use meow_ui::{
    component::{
        stack::VStack,
        text::{Text, TextContent},
        Callback, Component,
    },
    input::InputEvent,
};
use nx::mem::Shared;

use super::worker::WorkerChannelPair;

#[derive(Clone)]
pub enum LoggerEvent {
    Default,
    Message(Shared<dyn TextContent>),
    ChangeLastMessage(Shared<dyn TextContent>),
    TaskFinished,
}

impl Default for LoggerEvent {
    fn default() -> Self {
        Self::Default
    }
}

#[derive(Clone)]

pub struct LoggerTx {
    mpsc: thingbuf::mpsc::Sender<LoggerEvent>,
}

impl LoggerTx {
    fn new(mpsc: thingbuf::mpsc::Sender<LoggerEvent>) -> Self {
        Self { mpsc }
    }

    pub fn task_finished(&self) {
        let _ = self.mpsc.try_send(LoggerEvent::TaskFinished);
    }

    pub fn edit_last<T: TextContent + 'static>(&self, message: T) {
        let _ = self
            .mpsc
            .try_send(LoggerEvent::ChangeLastMessage(Shared::new(message)));
    }

    pub fn tx<T: TextContent + 'static>(&self, message: T) {
        let _ = self
            .mpsc
            .try_send(LoggerEvent::Message(Shared::new(message)));
    }
}

pub enum RxRes {
    Open(Option<LoggerEvent>),
    Closed,
}

pub struct LoggerRx {
    mpsc: thingbuf::mpsc::Receiver<LoggerEvent>,
}

impl LoggerRx {
    fn new(mpsc: thingbuf::mpsc::Receiver<LoggerEvent>) -> Self {
        Self { mpsc }
    }

    pub fn rx(&self) -> RxRes {
        match self.mpsc.try_recv() {
            Ok(val) => RxRes::Open(Some(val)),
            Err(thingbuf::mpsc::errors::TryRecvError::Empty) => RxRes::Open(None),
            Err(_) => RxRes::Closed,
        }
    }
}

pub fn channel() -> (LoggerTx, LoggerRx) {
    let (tx, rx) = thingbuf::mpsc::channel(256);
    (LoggerTx::new(tx), LoggerRx::new(rx))
}

pub struct LoggerTask {
    finished: bool,
    on_finish: Callback,
    on_exit: Callback,

    logs: Vec<Shared<dyn TextContent>>,

    max_lines_displayed: u32,

    ch: Rc<WorkerChannelPair>,
}

impl LoggerTask {
    pub fn new<X: Into<Callback> + 'static, Y: Into<Callback> + 'static>(
        ch: Rc<WorkerChannelPair>,
        on_finish: X,
        on_exit: Y,
        max_lines_displayed: u32,
    ) -> Self {
        Self {
            finished: false,
            max_lines_displayed,
            on_finish: on_finish.into(),
            on_exit: on_exit.into(),
            logs: Vec::new(),
            ch,
        }
    }

    fn process_task(&mut self) {
        if self.finished {
            return;
        }
        loop {
            match self.ch.rx.rx() {
                RxRes::Open(res) => match res {
                    Some(LoggerEvent::Message(msg)) => self.logs.push(msg),
                    Some(LoggerEvent::TaskFinished) => {
                        self.finished = true;
                        self.on_finish.call();
                        break;
                    }
                    Some(LoggerEvent::ChangeLastMessage(msg)) => match self.logs.last_mut() {
                        Some(text) => *text = msg,
                        None => self.logs.push(msg),
                    },
                    Some(_) => break,
                    None => break,
                },
                RxRes::Closed => {
                    self.finished = true;
                    // let _ = self.thread.join();
                    self.on_finish.call();
                    break;
                }
            }
        }
    }
}

impl Component for LoggerTask {
    fn x(&self) -> meow_ui::component::ComponentCoordiate {
        0
    }

    fn y(&self) -> meow_ui::component::ComponentCoordiate {
        0
    }

    fn draw_frame(
        &mut self,
        parent: (
            meow_ui::component::ComponentCoordiate,
            meow_ui::component::ComponentCoordiate,
        ),
        draw: &mut meow_ui::draw::Draw,
        theme: &meow_ui::themes::Theme,
    ) -> (
        meow_ui::component::ComponentSize,
        meow_ui::component::ComponentSize,
    ) {
        self.process_task();

        let mut vstack = VStack::new(0, 0);

        let start_idx = match self.logs.len() > self.max_lines_displayed as usize {
            true => self.logs.len() - self.max_lines_displayed as usize,
            false => 0,
        };

        for child in self.logs.iter().skip(start_idx) {
            vstack.children.push(Shared::new(Text::new_shared(
                child.clone(),
                0,
                0,
                meow_ui::component::text::TextSize::Small,
                false,
            )));
        }

        vstack.draw_frame(parent, draw, theme)
    }

    fn handle_event(&mut self, event: InputEvent) {
        if self.finished && event == InputEvent::B {
            self.on_exit.call();
            self.finished = false;
            self.logs = Vec::new();
        }
    }
}
