use meow_ui::{
    component::{
        button::TextButton, list::List, rectangle::FilledRectangle, text::Text, Component,
    },
    list,
};
use nx::mem::Shared;

use super::{
    strings::{TITLE_SD_BACKUP, TITLE_SD_INSTALL},
    MenuState,
};

pub struct MainMenu {
    list: List,
}

impl MainMenu {
    pub fn new(menu_text: Shared<Text>, menu_state: Shared<MenuState>) -> Self {
        let menu_text_1 = menu_text.clone();
        let menu_text_2 = menu_text.clone();
        let menu_text_3 = menu_text.clone();
        let menu_text_4 = menu_text;

        let menu_state_1 = menu_state.clone();
        let menu_state_2 = menu_state.clone();
        let menu_state_3 = menu_state.clone();
        let menu_state_4 = menu_state;
        Self {
            list: list![
                Shared::new(TextButton::new("Install from SD", 0, 0).on_click(move || {
                    menu_text_1.get().set_content(TITLE_SD_INSTALL);
                    *menu_state_1.get() = MenuState::InstallFromSDMenu
                })),
                Shared::new(TextButton::new("Backup to SD", 0, 0).on_click(move || {
                    menu_text_2.get().set_content(TITLE_SD_BACKUP);
                    *menu_state_2.get() = MenuState::BackupToSDSourceSelection
                })),
                Shared::new(FilledRectangle::new(0, 16, 64, 2, true)),
                Shared::new(TextButton::new("Credits", 0, 0).on_click(move || {
                    menu_text_3.get().set_content("Meow! Install - Credits");
                    *menu_state_3.get() = MenuState::Credits;
                })),
                Shared::new(TextButton::new("Exit", 0, 0).on_click(move || {
                    menu_text_4.get().set_content("Meow! Install - Exiting");
                    *menu_state_4.get() = MenuState::Exit;
                }))
            ]
            .selected_idx(0),
        }
    }
}

impl Component for MainMenu {
    fn x(&self) -> meow_ui::component::ComponentCoordiate {
        0
    }

    fn y(&self) -> meow_ui::component::ComponentCoordiate {
        0
    }

    fn draw_frame(
        &mut self,
        parent: (
            meow_ui::component::ComponentCoordiate,
            meow_ui::component::ComponentCoordiate,
        ),
        draw: &mut meow_ui::draw::Draw,
        theme: &meow_ui::themes::Theme,
    ) -> (
        meow_ui::component::ComponentSize,
        meow_ui::component::ComponentSize,
    ) {
        self.list.draw_frame(parent, draw, theme)
    }

    fn handle_event(&mut self, event: meow_ui::input::InputEvent) {
        self.list.handle_event(event)
    }

    fn selectable(&self) -> bool {
        true
    }
}
