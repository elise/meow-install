pub const TITLE_MAIN_MENU: &str = "Meow! Install";
pub const TITLE_SD_INSTALL: &str = "Meow! Install - SD Install";
pub const TITLE_SD_BACKUP: &str = "Meow! Install - Backup to SD";
pub const TITLE_DEST_SEL: &str = "Meow! Install - Select Destination";
pub const TITLE_SD_NCA_INSTALL: &str = "Meow! Install - Installing Unpacked NSP from SD";
pub const TITLE_SD_NSP_INSTALL: &str = "Meow! Install - Installing NSP from SD";
pub const TITLE_SD_NSP_BACKUP: &str = "Meow! Install - Backing Up NSP to SD";
pub const TITLE_SD_NCA_BACKUP: &str = "Meow! Install - Backing Up Unpacked NSP to SD";
