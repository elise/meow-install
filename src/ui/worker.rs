use alloc::{format, string::String};
use nx::result::Result;

use crate::{
    backup::sd::{
        nsp::backup_nsp_from_console_to_sd, unpacked::backup_unpacked_nsp_from_console_to_sd,
    },
    hos::hipc::ncm::ProgramId,
    install::sd::{handle_sd_install_nsp, handle_sd_install_unpacked_nca},
};

use super::{
    install::destination::StorageLocation,
    logger_task::{LoggerRx, LoggerTx},
};

pub fn worker_main(ch: &mut InternalWorkerChannelPair) -> Result<()> {
    while let RxRes::Open(res) = ch.rx.rx() {
        match res {
            Some(WorkerCommand::SDInstallUnpacked(path, storage_destination)) => {
                if let Err(why) =
                    handle_sd_install_unpacked_nca(path, storage_destination.into(), ch.tx.clone())
                {
                    ch.tx.tx(format!("Error: {}", why));
                }
                ch.tx.task_finished();
            }
            Some(WorkerCommand::SDInstallNSP(path, storage_destination)) => {
                if let Err(why) =
                    handle_sd_install_nsp(path.as_str(), storage_destination.into(), ch.tx.clone())
                {
                    ch.tx.tx(format!("Error: {}", why));
                }
                ch.tx.task_finished();
            }
            Some(WorkerCommand::SDBackupNSPConsole(name, program_id, source)) => {
                if let Err(why) =
                    backup_nsp_from_console_to_sd(&ch.tx, name, program_id, source.into())
                {
                    ch.tx.tx(format!("Error: {}", why));
                }
                ch.tx.task_finished();
            }
            Some(WorkerCommand::SDBackupUnpackedNSPConsole(name, program_id, source)) => {
                if let Err(why) =
                    backup_unpacked_nsp_from_console_to_sd(&ch.tx, name, program_id, source.into())
                {
                    ch.tx.tx(format!("Error: {}", why));
                }
                ch.tx.task_finished();
            }
            Some(WorkerCommand::Exit) => nx::thread::exit(),
            Some(WorkerCommand::None) => break,
            None => {}
        }

        nx::svc::sleep_thread(-1)?;
    }

    Ok(())
}

#[derive(Clone)]
pub enum WorkerCommand {
    None,
    SDInstallNSP(String, StorageLocation),
    SDInstallUnpacked(String, StorageLocation),
    SDBackupNSPConsole(String, ProgramId, StorageLocation),
    SDBackupUnpackedNSPConsole(String, ProgramId, StorageLocation),
    Exit,
}

impl Default for WorkerCommand {
    fn default() -> Self {
        Self::None
    }
}

#[derive(Clone)]

pub struct WorkerTx {
    mpsc: thingbuf::mpsc::Sender<WorkerCommand>,
}

impl WorkerTx {
    fn new(mpsc: thingbuf::mpsc::Sender<WorkerCommand>) -> Self {
        Self { mpsc }
    }

    pub fn tx(&self, message: WorkerCommand) {
        let _ = self.mpsc.try_send(message);
    }
}

enum RxRes {
    Open(Option<WorkerCommand>),
    Closed,
}

pub struct WorkerRx {
    mpsc: thingbuf::mpsc::Receiver<WorkerCommand>,
}

impl WorkerRx {
    fn new(mpsc: thingbuf::mpsc::Receiver<WorkerCommand>) -> Self {
        Self { mpsc }
    }

    fn rx(&self) -> RxRes {
        match self.mpsc.try_recv() {
            Ok(val) => RxRes::Open(Some(val)),
            Err(thingbuf::mpsc::errors::TryRecvError::Empty) => RxRes::Open(None),
            Err(_) => RxRes::Closed,
        }
    }
}

pub fn worker_channel(
    logger_tx: LoggerTx,
    logger_rx: LoggerRx,
) -> (InternalWorkerChannelPair, WorkerChannelPair) {
    let (worker_tx, worker_rx) = channel();
    (
        InternalWorkerChannelPair {
            tx: logger_tx,
            rx: worker_rx,
        },
        WorkerChannelPair {
            tx: worker_tx,
            rx: logger_rx,
        },
    )
}

fn channel() -> (WorkerTx, WorkerRx) {
    let (tx, rx) = thingbuf::mpsc::channel(16);
    (WorkerTx::new(tx), WorkerRx::new(rx))
}

pub struct InternalWorkerChannelPair {
    pub tx: LoggerTx,
    pub rx: WorkerRx,
}

pub struct WorkerChannelPair {
    pub tx: WorkerTx,
    pub rx: LoggerRx,
}
